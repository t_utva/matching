<?php 
namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\View;

class MatchingHelper extends Helper{

	var $helpers = ['Form'];

	public function convIncome($val, $type = 'toParam'){

		$incomeParam = Configure::read('income_option');

		if($type == "toParam"){
			$tmp = (int)($val / 10000);
			if(2 <= $tmp && $tmp <= 9){
				$return = $tmp;
			}elseif(10 <= $tmp){
				$return = 10;
			}else{
				$return = 1;
			}
		}

		return $return;

	}


	public function getImagePath($val = null, $noImage = '/images/user.png'){
		if(empty($val)){
			$return = $noImage;
		}else{
			$return = $val;
		}

		return $return;

	}

	// メッセージにて表示されるユーザー名
	public function getMessageUserName($userInfo){

		if(!empty($userInfo->firstname) && !empty($userInfo->lastname)){
			return $userInfo->firstname . ' ' . $userInfo->lastname;
		}elseif(!empty($userInfo->name)){
			return $userInfo->name;
		}

	}

	// 業者毎のユーザー特殊項目の生成
	public function generateForm($formParams, $formParamValues = []){

		$output = [];

		foreach($formParams as $param){

			$tmp = '<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">' . $param->label . '</label></div>';
			$tmp .= '<div class="col-md-10 col-sm-12 col-xs-12">';
			$tmp .= '<div class="form-group">';

			$options = explode('|', $param->options);
			$value = (!empty($this->request->data['form'][$param->form_code]))? $this->request->data['form'][$param->form_code] : '';

			switch($param->type){
				case '1':
					$tmp .= $this->Form->text('form.' . $param->form_code, ['value' => $value, 'class' => 'form-control']);
				break;
				case '2':
					$tmp .= $this->Form->textarea('form.' . $param->form_code, ['value' => $value, 'class' => 'form-control']);
				break;
				case '3':
					$tmp .= $this->Form->radio('form.' . $param->form_code, $options, ['value' => $value]);
				break;
				case '4':
					$tmp .= $this->Form->multiCheckbox('form.' . $param->form_code, $options);
				break;
				case '5':
					$tmp .= $this->Form->select('form.' . $param->form_code, $options, ['value' => $value, 'class' => 'form-control']);
				break;
			}

			$tmp .= '</div>';
			$tmp .= '</div>';
			$tmp .= '<div class="clearfix"></div>';

			$output[] = $tmp;
		}

		return implode('', $output);


	}



}



?>