<?php 
namespace App\Controller;

use Cake\Controller\Controller;
use App\Controller\AppController;

class MasterController extends AppController
{
	public function index(){
		$this->redirect('/master/agencies');
	}
}