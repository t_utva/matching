<?php 
namespace App\Controller;

use Cake\Controller\Controller;
use App\Controller\AppController;

class AgencyController extends AppController
{
	public function index(){
		$this->redirect('/agency/users');
	}
}