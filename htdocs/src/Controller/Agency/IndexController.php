<?php 
namespace App\Controller\Agency;

use Cake\Controller\Controller;
use App\Controller\AppController;

class IndexController extends AppController
{
	public function index(){
		$this->redirect('/agency/user');
	}
}