<?php 
namespace App\Controller\Agency;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class FormController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public $paginate = [
        'limit' => 20,
    ];



	public function index(){

		$agencyId = $this->Auth->user('id');

		$query = $this->Forms->find('formParams',['options' => ['agency_id' => $agencyId]]);
		$this->set('forms', $this->paginate($query));

		$this->setPolicy('agency');
		$this->setTitle('会員設定項目');
		$this->setSubTitle('設定項目一覧');
	}

	public function add(){
		$this->setPolicy('agency');
		$this->setTitle('会員設定項目');
		$this->setSubTitle('会員設定項目追加');

		$form = $this->Forms->newEntity();

		// postがあればentityアップデートして保存
		if($this->request->is('post')){
			$this->request->data['form_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));
			$this->Forms->patchEntity($form, $this->request->data);

			$agencyId = $this->Auth->user('id');
			$form->agency_id = $agencyId;

			if($this->Forms->save($form)){
				$this->redirect('/agency/form/');
				//$this->Flash->success('追加されました。');
			}
		}

		$this->set('form', $form);
		$this->render('form');

	}


	public function edit($form_code = null){

		$form = $this->Forms->find()->where(['form_code' => $form_code])->first();

		// postがあればentityアップデートして保存
		if($this->request->data){

			$this->Forms->patchEntity($form, $this->request->data);

			if($this->Forms->save($form)){
				$this->Flash->success('変更されました。');
			}
		}

		$this->set('form', $form);

		$this->setPolicy('agency');
		$this->setTitle('会員設定項目');
		$this->setSubTitle('会員設定項目更新');
		$this->render('form');
	}

	public function delete($code){
		$this->autoRender = false;
		$agencyId = $this->Auth->user('id');

		$entity = $this->Forms->find()->where(['form_code' => $code, 'agency_id' => $agencyId])->first();
		if($entity){
			$this->Forms->deleteAll(['form_code' => $code, 'agency_id' => $agencyId]);
			$this->FormValues->deleteAll(['form_code' => $code]);
			$this->Flash->success('項目を削除しました。');
		}

		$this->redirect('/agency/form/');
	}


}