<?php 
namespace App\Controller\Agency;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class MatchingController extends AppController
{

	/**
	 * 認証不要なアクションを定義
	 */
	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
	}

	/**
	 * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
	 */
	public function isAuthorized($user)
	{
		return true;
	}

	public function index(){

		$userId = $this->Auth->user('id');
		$matchings = $this->Matchings->find('indexList',['options' => ['applicant_user_agency_id' => $userId]])->toArray();

		foreach($matchings as $k => $matching){
			$applicant_user = $this->Users->find('detail', ['options' => ['id' => $matching['applicant_user_id']]])->first()->toArray();
			$passive_user = $this->Users->find('detail', ['options' => ['id' => $matching['passive_user_id']]])->first()->toArray();
			$matchings[$k]['applicant_user'] = $applicant_user;
			$matchings[$k]['passive_user'] = $passive_user;
		}

		$this->set('matchings', $matchings);
		$this->setPolicy('agency');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合い申し込み一覧');
	}

	public function edit($matching_code = null){

		$matching = $this->Matchings->find('detail', ['options' => ['matching_code' => $matching_code]])->first();

		if(!empty($this->request->data)){
			$this->Matchings->patchEntity($matching, $this->request->data);

			if($this->Matchings->save($matching)){
				$this->Flash->success('変更されました。');
			}
		}

		$this->set('matching', $matching);
		$this->setPolicy('agency');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合申込詳細');
	}
}