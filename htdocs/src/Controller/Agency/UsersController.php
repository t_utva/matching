<?php 
namespace App\Controller\Agency;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Hash;

class UsersController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public $paginate = [
        'limit' => 20,
    ];

	public function index(){

		$agencyId = $this->Auth->user('id');

		$query = $this->Users->find()->where(['agency_id' => $agencyId]);
		$this->set('users', $this->paginate($query));
		
		$this->setPolicy('agency');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員情報一覧');
	}

	public function result(){

		if($this->request->data){
			$this->Session->write('Agency.SearchParam', $this->request->data);
		}else{
			$this->request->data = $this->Session->read('Agency.SearchParam');
		}

		$datas = $this->request->data;

		$agencyId = $this->Auth->user('id');
		$datas['userModel']['agency_id'] = $agencyId;
		$datas['userModel']['id'] = 0;

		$this->paginate = [
			'finder' => [
				'indexList' => ['options' => $datas]
			]
		];

		$usersEntities = $this->paginate($this->Users);

		$this->set('users', $usersEntities);

		$this->setPolicy('agency');
		$this->setTitle('会員検索');
		$this->setSubTitle('検索結果');
		$this->render('index');
	}





	public function add(){
		$this->setPolicy('agency');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員新規追加');

		$agencyId = $this->Auth->user('id');
		$user = $this->Users->newEntity();
		$agency = $this->Agencies->find('detail',['options' => ['id' => $agencyId]])->first();

		// postがあればentityアップデートして保存
		if(!empty($this->request->data)){
			$this->request->data['user_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));

			if(!empty($this->request->data['image']['tmp_name'])){
				$dir = WWW_ROOT . "upload/profile/" . $agency->agency_code;
				$file = $this->file_upload($this->request->data['image'], $dir);
				$this->request->data['image'] = '/upload/profile/' . $agency->agency_code . '/' . $file;
			}elseif(!empty($this->request->data['image_del'])){
				$this->request->data['image'] = '';
			}else{
				unset($this->request->data['image']);
			}

			$this->Users->patchEntity($user, $this->request->data);

			$user->agency_id = $agencyId;

			if($this->Users->save($user)){
				$this->saveUserAttachmentValue($user->id);
				$this->Flash->success('追加されました。');
			}
		}

		$this->set('user', $user);

		// 業者毎項目のロード
		$this->setUserAttachmentForm($agencyId);
		$this->render('form');
	}


	public function edit($user_code = null){

		$user = $this->Users->find()->where(['user_code' => $user_code])->first();
		$agencyId = $this->Auth->user('id');
		$agency = $this->Agencies->find('detail',['options' => ['id' => $agencyId]])->first();

		// postがあればentityアップデートして保存
		if(!empty($this->request->data)){

			if(empty($this->request->data['password'])){
				unset($this->request->data['password']);
			}

			if(!empty($this->request->data['image']['tmp_name'])){
				$dir = WWW_ROOT . "upload/profile/" . $agency->agency_code;

				$file = $this->file_upload($this->request->data['image'], $dir);
				$this->request->data['image'] = '/upload/profile/' . $agency->agency_code . '/' . $file;
			}

			$this->Users->patchEntity($user, $this->request->data);

			if($this->Users->save($user)){
				$this->saveUserAttachmentValue($user->id);
				$this->Flash->success('変更されました。');
			}
		}else{
			$this->request->data = $user;
			$this->request->data['form'] = $this->loadUserAttachmentValue($user->id);
		}

		$this->set('user', $user);

		// 業者毎項目のロード
		$this->setUserAttachmentForm($agencyId);
		$this->setPolicy('agency');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員詳細情報');
		$this->render('form');
	}

	public function message($user_code){

		// userのデータを取得
		$user = $this->Users->find('detail',['options' => ['user_code' => $user_code]])->toArray();

		// チャットルーム情報を取得
		$userId = $this->Auth->user('id');

		$rooms = $this->MessageRoomUsers->find('memberRoomList', ['options' => ['member_id' => $userId, 'member_type' => 'agency']])->toArray();

		$tmp = [];
		foreach($rooms as $room){
			$tmp[] = $room->message_room;
		}
		$rooms = $tmp;

		$exists = false;
		$roomName = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));

		foreach($rooms as $room){
			if(Hash::extract($room, 'message_room_users.{n}[member_id=' . $user[0]->id . ']')){
				$exists = true;
				$roomName = $room['name'];
			}
		}

		// ルームがなければルーム情報のみ登録
		if(!$exists){
			$saveData = [
				'name' => $roomName,
				'message_room_users' => [
					['member_type' => 'agency', 'member_id' => $userId],
					['member_type' => 'user', 'member_id' => $user[0]->id],
				]
			];

			$entity = $this->MessageRooms->newEntity();
			$entity = $this->MessageRooms->patchEntity($entity, $saveData, ['associated' => ['MessageRoomUsers']]);
			$this->MessageRooms->save($entity);
		}

		return $this->redirect('/agency/messages/message/' . $roomName);
	}

	public function uploadCsv(){

		$agencyId = $this->Auth->user('id');
		$datas = $this->UserCsvs->find()->where(['agency_id' => $agencyId]);


		$this->set('datas', $datas);
		$this->setPolicy('agency');
		$this->setTitle('会員登録');
		$this->setSubTitle('会員CSV登録申請');
	}

	public function uploadCsvConfirm(){

		if(empty($this->request->data) || empty($this->request->data['upload'])){
			$this->redirect('/agency/users/upload_csv');
		}

		$dir = realpath(WWW_ROOT . "upload/upload_csv");
		$this->allowedFileFormat = ['text/csv', 'text/plain'];
		$file = $this->file_upload($this->request->data['upload'], $dir);

		$dataset = $this->importCSV($dir . '/' . $file);

		$this->Session->write('Agency.upload_csv_name', $file);

		$this->set('dataset', $dataset);
		$this->setPolicy('agency');
		$this->setTitle('会員登録');
		$this->setSubTitle('会員CSV登録申請');
	}

	public function uploadCsvComplete(){

		$file 	 = $this->Session->read('Agency.upload_csv_name');
		$agencyId = $this->Auth->user('id');

		$entity = $this->UserCsvs->newEntity();

		// postがあればentityアップデートして保存
		if($this->request->is('post')){
			$data = [
				'agency_id' => $agencyId,
				'file'		=> $file,
			];

			$this->UserCsvs->patchEntity($entity, $data);

			if($this->UserCsvs->save($entity)){
				$this->Flash->success('申請が送信されました。');
			}
		}

		$this->setPolicy('agency');
		$this->setTitle('会員登録');
		$this->setSubTitle('会員CSV登録申請');

	}

	public function deleteCsv($id){
		$this->autoRender = false;
		$agencyId = $this->Auth->user('id');

		$entity = $this->UserCsvs->find()->where(['id' => (int)$id, 'agency_id' => $agencyId])->first();
		if($entity){
			$dir = realpath(WWW_ROOT . "upload/upload_csv");
			unlink($dir . '/' . $entity->file);
			$this->UserCsvs->deleteAll(['id' => (int)$id, 'agency_id' => $agencyId]);
			$this->Flash->success('ファイルを削除しました。');
		}

		$this->redirect('/agency/users/upload_csv');
	}

	public function delete($user_code = null){
		$this->setPolicy('agency');
		$user = $this->Users->find()->where(['user_code' => $user_code])->first();

		$agencyId = $this->Auth->user('id');

		if(!empty($user) && $agencyId == $user->agency_id){
			$this->Flash->success('ユーザー「' . $user->firstname . $user->lastname . '」を削除しました。');
			$this->Users->delete($user);
		}else{
			$this->Flash->success('削除できませんでした。');
		}

		$this->redirect('/agency/users/');
	}


}