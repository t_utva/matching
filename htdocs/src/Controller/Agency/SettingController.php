<?php 
namespace App\Controller\Agency;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Hash;

class SettingController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }



	public function edit(){

        $agencyId = $this->Auth->user('id');
        $agency = $this->Agencies->find('detail', ['options' => ['id' => $agencyId]])->first();

        // postがあればentityアップデートして保存
        if($this->request->data){

            if(empty($this->request->data['password'])){
                unset($this->request->data['password']);
            }

            $this->Agencies->patchEntity($agency, $this->request->data);

            if($this->Agencies->save($agency)){
                $this->Flash->success('変更されました。');
            }
        }




        $this->set('agency', $agency);
		$this->setPolicy('agency');
		$this->setTitle('基本設定');
		$this->setSubTitle('基本設定');
	}

}