<?php 
namespace App\Controller\Master;

use Cake\Controller\Controller;
use App\Controller\AppController;

class IndexController extends AppController
{
	public function index(){
		$this->redirect('/master/user');
	}
}