<?php 
namespace App\Controller\Master;

use Cake\Controller\Controller;
use App\Controller\AppController;

class SettingController extends AppController
{
	public function edit(){
		$this->setPolicy('master');
		$this->setTitle('基本設定');
		$this->setSubTitle('基本設定');
	}

	public function confirm(){
		$this->setPolicy('master');
		$this->setTitle('基本設定');
		$this->setSubTitle('基本設定 - 確認');
	}

	public function complete(){
		$this->setPolicy('master');
		$this->setTitle('基本設定');
		$this->setSubTitle('基本設定');
	}


}