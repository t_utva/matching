<?php 
namespace App\Controller\Master;

use Cake\Controller\Controller;
use App\Controller\AppController;

class ProfileController extends AppController
{
	public function edit(){
		$this->setPolicy('master');
		$this->setTitle('相談所プロフィール');
		$this->setSubTitle('相談所プロフィール設定');
	}

	public function confirm(){
		$this->setPolicy('master');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール編集 - 確認');
	}

	public function complete(){
		$this->setPolicy('master');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール');
	}


}