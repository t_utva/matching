<?php 
namespace App\Controller\Master;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Utility\Hash;

class MessagesController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public function message($roomName){
		$userId = $this->Auth->user('id');

		$rooms = $this->MessageRoomUsers->find('memberRoomList', ['options' => ['member_id' => $userId, 'member_type' => 'agency']])->toArray();

		//データ整形
		$tmp = [];
		foreach($rooms as $room){
			$tmp[] = $room->message_room;
		}
		$rooms = $tmp;

		// ルーム名取得
		foreach($rooms as $k => $room){
			if($room['name'] == $roomName){
				$roomId = $room['id'];
				$roomName = $room['name'];
			}

			foreach($room['message_room_users'] as $key => $users){
				if($users->member_id == $userId && $users->member_type == 'agency') continue;

				$user = $this->Users->find('detail',['options' => ['id' => $users->member_id]])->toArray();
				$rooms[$k]['userInfo'] = $user;
			}
		}

		// メッセージデータ取得
		$messageContent = $this->MessageContents->find('messageContent', ['options' => ['message_room_id' => $roomId, 'limit' => 30]])->toArray();

		$this->set('userId', $userId);
		$this->set('rooms', $rooms);
		$this->set('contents', $messageContent);
		$this->set('roomName', $roomName);

		$this->setPolicy('master');
		$this->setTitle('メッセージ');
		$this->setSubTitle('メッセージ一覧');
	}


	public function post(){
		$this->autoRender = false;

		$userId = $this->Auth->user('id');

		// ルーム情報からルームID取得
		$messageRoom = $this->MessageRooms->findByName($this->request->data['room'])->toArray();

		// コンテンツ保存用配列
		$saveData = [
			'message_room_id' => $messageRoom[0]->id,
			'message_room_user_id' => $userId,
			'body' => $this->request->data['message']
		];

		// メッセージコンテンツへ保存
		$entity = $this->MessageContents->newEntity();
		$entity = $this->MessageContents->patchEntity($entity, $saveData);
		if($this->MessageContents->save($entity)){
			$messageRoomEntity = $this->MessageRooms->get($messageRoom[0]->id);

			// メッセーイジの保存に成功したらルームの更新日変更
			$this->MessageRooms->touch($messageRoomEntity,'Model.beforeSave');
			$this->MessageRooms->save($messageRoomEntity);

		}

		// 保存したユーザー情報取得
		$user = $this->Users->findById($userId)->toArray();

		$image = (!empty($user[0]->image))? $user[0]->image : '/images/user.png';

		$return = [
			'user_code' => $user[0]->user_code,
			'image' 	=> $image,
			'firstname' => $user[0]->firstname,
			'lastname' 	=> $user[0]->lastname
		];

		echo json_encode($return);
		exit;

	}



}