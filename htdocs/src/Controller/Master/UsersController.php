<?php 
namespace App\Controller\Master;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Hash;


class UsersController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public $paginate = [
        'limit' => 20,
    ];

	public function index(){

		$agencyId = $this->Auth->user('id');

		$query = $this->Users->find()->contain(['Agencies']);
		$this->set('users', $this->paginate($query));

		$this->setPolicy('master');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員情報一覧');
	}

	public function add(){
		$this->setPolicy('master');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員新規追加');

		$agencyId = $this->Auth->user('id');
		$user = $this->Users->newEntity();
		$agency = $this->Agencies->find('detail',['options' => ['id' => $agencyId]])->first();

		// postがあればentityアップデートして保存
		if($this->request->is('post')){
			$this->request->data['user_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));
			if(!empty($this->request->data['image']['tmp_name'])){
				$dir = WWW_ROOT . "upload/profile/" . $agency->agency_code;

				$file = $this->file_upload($this->request->data['image'], $dir);
				$this->request->data['image'] = '/upload/profile/' . $agency->agency_code . '/' . $file;
			}



			$this->Users->patchEntity($user, $this->request->data);

			$user->agency_id = $agencyId;

			if($this->Users->save($user)){
				$this->saveUserAttachmentValue($user->id);
				$this->Flash->success('追加されました。');
			}
		}


		$this->set('user', $user);

		//業者リスト取得
		$agencies = $this->Agencies->find('list');
		$this->set('agencies', $agencies);

		// 業者毎項目のロード
		$this->setUserAttachmentForm($agencyId);
		$this->render('form');
	}


	public function edit($user_code = null){

		$user = $this->Users->find()->where(['user_code' => $user_code])->contain(['Agencies'])->first();
		$agencyId = $user->agency_id;
		$agency = $this->Agencies->find('detail',['options' => ['id' => $agencyId]])->first();

		// postがあればentityアップデートして保存
		if(!empty($this->request->data)){

			if(empty($this->request->data['password'])){
				unset($this->request->data['password']);
			}

			if(!empty($this->request->data['image']['tmp_name'])){
				$dir = WWW_ROOT . "upload/profile/" . $agency->agency_code;
				$file = $this->file_upload($this->request->data['image'], $dir);
				$this->request->data['image'] = '/upload/profile/' . $agency->agency_code . '/' . $file;
			}elseif(!empty($this->request->data['image_del'])){
				$this->request->data['image'] = '';
			}else{
				unset($this->request->data['image']);
			}

			$this->Users->patchEntity($user, $this->request->data);

			if($this->Users->save($user)){
				$this->saveUserAttachmentValue($user->id);
				$this->Flash->success('変更されました。');
			}
		}else{
			$this->request->data = $user;
			$this->request->data['form'] = $this->loadUserAttachmentValue($user->id);
		}


		$this->set('user', $user);

		//業者リスト取得
		$agencies = $this->Agencies->find('list');
		$this->set('agencies', $agencies);

		// 業者毎項目のロード
		$this->setUserAttachmentForm($agencyId);
		$this->setPolicy('master');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員詳細情報');
		$this->render('form');
	}

	public function import(){

		$datas = $this->UserCsvs->find()->contain(['Agencies']);

		$this->set('datas', $datas);
		$this->setPolicy('master');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員一括登録');
	}

	public function importComplete($mode, $id){
		$this->autoRender = false;

		if($mode == null || $id == null){
			$this->redirect('/master/users/import');
		}

		$csvEntity = $this->UserCsvs->find()->where(['id' => $id])->first();

		if($mode == 'approve'){

			$dir = realpath(WWW_ROOT . "upload/upload_csv");
			$dataset = $this->importCSV($dir . '/' . $csvEntity->file);

			foreach($dataset as $k => $data){
				if($k == 0) continue;

				$patchData = $this->convertCsv($data);
				$patchData['agency_id'] = $csvEntity->agency_id;
				$patchData['user_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));

				$entity = $this->Users->newEntity();
				$this->Users->patchEntity($entity, $patchData);
				$this->Users->save($entity);
			}

			$entity = ['status' => 1];
			$this->Flash->success('登録しました。');
		}else{
			$entity = ['status' => 2];
			$this->Flash->success('却下しました。');
		}
		$this->UserCsvs->patchEntity($csvEntity, $entity);
		$this->UserCsvs->save($csvEntity);

		$this->redirect('/master/users/import');
	}

	private function convertCsv($data){
		$return = [
			'login' => $data[0],
			'email' => $data[0],
			'password' => $data[1],
			'role'	=> null,
			'status' => 1,
			'image' => null,
			'firstname' => $data[2],
			'lastname' => $data[3],
			'firstname_kana' => $data[4],
			'lastname_kana' => $data[5],
			'sex' => $data[6],
			'birthday' => $data[7],
			'height' => $data[8],
			'weight' => $data[9],
			'blood_type' => $data[10],
			'edu_back' => $data[11],
			'edu_back_type' => $data[12],
			'income' => $data[13],
			'live_pref' => $data[14],
			'live_city' => $data[15],
			'live_address' => $data[16],
			'relationship' => $data[17],
			'job'			=> $data[18],
			'work_pref'		=> $data[19],
			'work_city'	    => $data[20],
			'work_remarks' => $data[21],
			'nationality' => $data[22],
			'history' => $data[23],
			'reason' => $data[24],
			'children' => $data[25],
			'hobby' => $data[26],
			'qualification' => $data[27],
			'asset' => $data[28],
			'asset_other' => $data[29],
			'alcohol' => $data[30],
			'tobacco' => $data[31],
			'religion' => $data[32],
			'religion_name' => $data[33],
			'desired_message' => $data[34],
			'desired_age' => $data[35],
			'desired_salary' => $data[36],
			'desired_pref' => $data[37],
			'desired_city' => $data[38],
			'desired_family01' => $data[39],
			'desired_family02' => $data[40],
			'about_child' => $data[41],
			'about_adopted' => $data[42],
			'about_remarrior' => $data[43],
			'pr02' => $data[44]
		];

		return $return;
	}



	public function delete($user_code = null){
		$this->setPolicy('master');
		$user = $this->Users->find()->where(['user_code' => $user_code])->first();

		if(!empty($user)){
			$this->Flash->success('ユーザー「' . $user->firstname . $user->lastname . '」を削除しました。');
			$this->Users->delete($user);
		}else{
			$this->Flash->success('削除できませんでした。');
		}


		$this->redirect('/master/users/');
	}


}