<?php 
namespace App\Controller\Master;
use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Auth\DefaultPasswordHasher;

class LoginController extends AppController
{


	public function index(){

		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}else{
				$this->Flash->error(__('Invalid username or password, try again'));
			}
		}

		$this->setPolicy('master');
		$this->setTitle('管理者ログイン');
		$this->viewBuilder()->layout('login');
	}

	public function logout()
	{
		$this->Session->destroy(); // セッションの破棄
		return $this->redirect($this->Auth->logout()); // ログアウト処理
	}

}