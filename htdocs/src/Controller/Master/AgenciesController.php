<?php 
namespace App\Controller\Master;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Hash;


class AgenciesController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public $paginate = [
        'limit' => 20,
    ];

	public function index(){

		$query = $this->Agencies->find();
		$this->set('agencies', $this->paginate($query));

		$this->setPolicy('master');
		$this->setTitle('団体情報');
		$this->setSubTitle('団体情報一覧');
	}

	public function add(){
		$this->setPolicy('master');
		$this->setTitle('団体情報');
		$this->setSubTitle('団体新規追加');

		$agency = $this->Agencies->newEntity();

		// postがあればentityアップデートして保存
		if($this->request->is('post')){
			$this->request->data['agency_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));
			$this->Agencies->patchEntity($agency, $this->request->data);

			if($this->Agencies->save($agency)){
				$this->Flash->success('追加されました。');
			}
		}

		$this->set('agency', $agency);
		$this->render('form');
	}


	public function edit($agency_code = null){

		$agency = $this->Agencies->find()->where(['agency_code' => $agency_code])->first();

		// postがあればentityアップデートして保存
		if(!empty($this->request->data)){

			if(empty($this->request->data['password'])){
				unset($this->request->data['password']);
			}

			$this->Agencies->patchEntity($agency, $this->request->data);

			if($this->Agencies->save($agency)){
				$this->Flash->success('変更されました。');
			}
		}


		$this->set('agency', $agency);
		$this->setPolicy('master');
		$this->setTitle('団体情報');
		$this->setSubTitle('団体詳細情報');
		$this->render('form');
	}

	public function delete(){
		$this->setPolicy('master');
		$this->setTitle('団体情報');
		$this->setSubTitle('団体詳細情報');
	}


}