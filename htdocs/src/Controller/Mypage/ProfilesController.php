<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class ProfilesController extends AppController
{


	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

	public function index(){
		//$this->autoRender = false;
		$this->setPolicy('user');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール');

		// userのデータを取得
		$userId = $this->Auth->user('id');
		$user = $this->Users->find('all')->where(['Users.id' => $userId])->contain(['Agencies'])->toArray();
		$this->set('user', $user[0]);
	}

	public function edit(){
		$this->setPolicy('user');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール編集');

		$userId = $this->Auth->user('id');
		$user = $this->Users->findById($userId)->contain(['Agencies'])->first();

		// postがあればentityアップデートして保存
		if(!empty($this->request->data)){

			if(empty($this->request->data['password'])){
				unset($this->request->data['password']);
			}

			if(!empty($this->request->data['image']['tmp_name'])){
				$dir = WWW_ROOT . "upload/profile/" . $user->agency->agency_code;

				$file = $this->file_upload($this->request->data['image'], $dir);
				$this->request->data['image'] = '/upload/profile/' . $user->agency->agency_code . '/' . $file;
			}elseif(!empty($this->request->data['image_del'])){
				$this->request->data['image'] = '';
			}else{
				unset($this->request->data['image']);
			}
			
			$this->Users->patchEntity($user, $this->request->data);

			if($this->Users->save($user)){
				$this->saveUserAttachmentValue($userId);
				$this->Flash->success('変更されました。');
			}
		}else{
			$this->request->data = $user;
			$this->request->data['form'] = $this->loadUserAttachmentValue($userId);
		}

		$this->setUserAttachmentForm($user->agency_id);

		$this->set('user', $user);
		$this->setPolicy('user');

	}

	public function confirm(){
		$this->setPolicy('user');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール編集 - 確認');
	}

	public function complete(){
		$this->setPolicy('user');
		$this->setTitle('プロフィール');
		$this->setSubTitle('マイプロフィール');
	}


}