<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;

class IndexesController extends AppController
{
	public function index(){
		$this->redirect('/mypage/profile');
	}
}