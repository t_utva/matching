<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class MatchingController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }

    public function index(){

    	$userId = $this->Auth->user('id');
    	$matchings = $this->Matchings->find('indexList',['options' => ['applicant_user_id' => $userId]])->toArray();

    	$this->set('matchings', $matchings);
		$this->setPolicy('user');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合い申し込み一覧');
    }


	public function apply(){
		$this->setPolicy('user');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合い申し込み');
	}

	public function applyConfirm(){

		// userのデータを取得
		$user_code = $this->request->data['user_code'];
		$user = $this->Users->find('detail',['options' => ['user_code' => $user_code]])->toArray();

		if($this->request->is('post')){
			$userId = $this->Auth->user('id');

			$applicant_user = $this->Users->find()->where(['id' => $userId])->first();
			$passive_user = $this->Users->find()->where(['user_code' => $this->request->data['user_code']])->first();

			$this->request->data['matching_code'] = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));
			$this->request->data['applicant_user_id'] = $applicant_user['id'];
			$this->request->data['applicant_user_agency_id'] = $applicant_user['agency_id'];
			$this->request->data['passive_user_id'] = $passive_user['id'];
			$this->request->data['passive_user_agency_id'] = $passive_user['agency_id'];

			$entity = $this->Matchings->newEntity();
			$this->Matchings->patchEntity($entity, $this->request->data);
			if($this->Matchings->save($entity)){
				$this->redirect('/mypage/matching/apply_complete');
			}
		}


		$this->set('user', $user);
		$this->setPolicy('user');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合い申し込み');
	}

	public function applyComplete(){

		$this->setPolicy('user');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合い申し込み');
	}

	public function edit($matching_code = null){

		$matching = $this->Matchings->find('detail', ['options' => ['matching_code' => $matching_code]])->first();
		$this->set('matching', $matching);
		$this->setPolicy('user');
		$this->setTitle('プロフィール検索');
		$this->setSubTitle('お見合申込詳細');
	}

}