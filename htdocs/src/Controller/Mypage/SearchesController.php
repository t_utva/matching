<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class SearchesController extends AppController
{

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Users.last_login_date' => 'desc'
        ]
    ];

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
	}


    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }


	public function index(){

		$this->setPolicy('user');
		$this->setTitle('会員検索');
		$this->setSubTitle('会員検索');
	}

	public function result(){

		if($this->request->data){
			$this->Session->write('Mypage.SearchParam', $this->request->data);
		}else{
			$this->request->data = $this->Session->read('Mypage.SearchParam');
		}

		$datas = $this->request->data;

		$userId = $this->Auth->user('id');
		$user = $this->Users->get($userId)->toArray();
		$datas['userModel'] = $user;

		$this->paginate = [
			'finder' => [
				'indexList' => ['options' => $datas]
			]
		];

		$usersEntities = $this->paginate($this->Users);

		$this->set('users', $usersEntities);

		$this->setPolicy('user');
		$this->setTitle('会員検索');
		$this->setSubTitle('検索結果');
	}

	public function detail(){
		$this->setPolicy('user');
		$this->setTitle('会員検索');
		$this->setSubTitle('会員情報詳細');
	}

	public function contact(){
		$this->setPolicy('user');
	}



}