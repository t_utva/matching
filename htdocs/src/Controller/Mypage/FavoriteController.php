<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;

class FavoriteController extends AppController
{
	public function index(){
		$this->setPolicy('user');
		$this->setTitle('お気に入り');
		$this->setSubTitle('お気に入り一覧');
	}
}