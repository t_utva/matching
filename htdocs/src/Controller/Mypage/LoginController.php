<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;

class LoginController extends AppController
{

	public function index(){
		$this->setPolicy('user');
		$this->setTitle('マイページログイン');
		$this->viewBuilder()->layout('login');
	}

	public function logout()
	{
		$this->Session->destroy(); // セッションの破棄
		return $this->redirect($this->Auth->logout()); // ログアウト処理
	}

}