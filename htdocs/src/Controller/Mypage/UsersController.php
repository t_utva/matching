<?php 
namespace App\Controller\Mypage;

use Cake\Controller\Controller;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Utility\Hash;


class UsersController extends AppController
{

	/**
     * 認証不要なアクションを定義
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * role別にアクセスを制御したい場合はここに記述。全ロールに許可する場合はreturn trueとだけ書く
     */
    public function isAuthorized($user)
    {
        return true;
    }


	public function login(){

		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}else{
				$this->Flash->error(__('Invalid username or password, try again'));
			}
		}

		$this->setPolicy('user');
		$this->setTitle('マイページログイン');
		$this->viewBuilder()->layout('login');
	}

	public function logout()
	{
		$this->Session->destroy(); // セッションの破棄
		return $this->redirect($this->Auth->logout()); // ログアウト処理
	}

	public function index(){
		$this->setPolicy('user');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員詳細情報');
	}

	public function detail($user_code){

		// userのデータを取得
		$user = $this->Users->find('detail',['options' => ['user_code' => $user_code]])->toArray();

		// チャットルーム情報を取得
		$userId = $this->Auth->user('id');

		$rooms = $this->MessageRoomUsers->find('memberRoomList', ['options' => ['member_id' => $userId, 'member_type' => 'user']])->toArray();
		$tmp = [];
		foreach($rooms as $room){
			$tmp[] = $room->message_room;
		}
		$rooms = $tmp;

		$exists = false;
		$roomName = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999));

		foreach($rooms as $room){
			if(Hash::extract($room, 'message_room_users.{n}[member_id=' . $user[0]->id . ']')){
				$exists = true;
				$roomName = $room['name'];
			}
		}

		// ルームがなければルーム情報のみ登録
		if(!$exists){
			$saveData = [
				'name' => $roomName,
				'message_room_users' => [
					['member_type' => 'user', 'member_id' => $userId],
					['member_type' => 'user', 'member_id' => $user[0]->id],
				]
			];

			$entity = $this->MessageRooms->newEntity();
			$entity = $this->MessageRooms->patchEntity($entity, $saveData, ['associated' => ['MessageRoomUsers']]);
			$this->MessageRooms->save($entity);
		}

		$this->set('user', $user[0]);
		$this->set('msgRoomName', $roomName);
		$this->setPolicy('user');
		$this->setTitle('会員情報');
		$this->setSubTitle('会員詳細情報');

	}



}