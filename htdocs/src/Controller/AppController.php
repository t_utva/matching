<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use RuntimeException;
use \SplFileObject;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

	protected $applicationSalt = 'SUEDI4bdKg2MJykUGHIudQe7zld0RjLE';

	protected $allowedFileFormat = [
									'jpg' => 'image/jpeg',
									'png' => 'image/png',
									'gif' => 'image/gif'
									];

	public $helpers = [
		'Paginator' => ['templates' => 'paginator-templates']
	];

	/**
	 * Initialization hook method.
	 *
	 * Use this method to add common initialization code like loading components.
	 *
	 * e.g. `$this->loadComponent('Security');`
	 *
	 * @return void
	 */
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

		switch($this->request->prefix){
			case 'master' : 
				$this->loadComponent('Auth', [
					'authorize'      => [ 'Controller'],
					'loginAction'    => '/master/login/',
					'loginRedirect'  => [ 'controller' => 'Users' , 'action' => 'index' ],
					'logoutRedirect' => [ 'controller' => 'Login' , 'action' => 'index' ],
					'authError' => 'ログインしてください',
					'authenticate'   => [ 'Form' => ['userModel' => 'Masters', 'fields' => ['username' => 'login_id', 'password' => 'password' ] ] ],
				]);

				$this->Auth->sessionKey = 'Master';
			break;
			case 'agency' : 
				$this->loadComponent('Auth', [
					'authorize'      => [ 'Controller'],
					'loginAction'    => '/agency/login/',
					'loginRedirect'  => [ 'controller' => 'Users' , 'action' => 'index' ],
					'logoutRedirect' => [ 'controller' => 'Login' , 'action' => 'index' ],
					'authError' => 'ログインしてください',
					'authenticate'   => [ 'Form' => ['userModel' => 'Agencies', 'fields' => ['username' => 'login_id', 'password' => 'password' ] ] ],
				]);

				$this->Auth->sessionKey = 'Agency';

			break;
			case 'mypage' :
				$this->loadComponent('Auth', [
					'authorize'      => [ 'Controller'],
					'loginAction'    => '/mypage/users/login',
					'loginRedirect'  => [ 'controller' => 'Profiles' , 'action' => 'index' ],
					'logoutRedirect' => '/mypage/users/login',
					'authError' => 'ログインしてください',
					'authenticate'   => [ 'Form' => [ 'fields' => ['username' => 'email', 'password' => 'password' ] ] ],
				]);

				$this->Auth->sessionKey = 'Mypage';

			break;
		}

		$this->Session = $this->request->session();

		$this->Agencies 		= TableRegistry::get('Agencies');
		$this->Users 			= TableRegistry::get('Users');
		$this->UserCsvs 		= TableRegistry::get('UserCsvs');
		$this->Forms 			= TableRegistry::get('Forms');
		$this->FormValues 		= TableRegistry::get('FormValues');
		$this->Matchings 		= TableRegistry::get('Matchings');
		$this->MessageRooms 	= TableRegistry::get('MessageRooms');
		$this->MessageRoomUsers = TableRegistry::get('MessageRoomUsers');
		$this->MessageContents 	= TableRegistry::get('MessageContents');
		$this->MessageContentRecipients = TableRegistry::get('MessageContentRecipientsTable');
	}

	public function isAuthorized($user)
	{
		// Admin can access every action
		if (isset($user['role']) && $user['role'] === 'admin') {
			return true;
		}

		// Default deny
		return false;
	}

	/**
	 * Before render callback.
	 *
	 * @param \Cake\Event\Event $event The beforeRender event.
	 * @return \Cake\Http\Response|null|void
	 */
	public function beforeRender(Event $event)
	{
		// Note: These defaults are just to get started quickly with development
		// and should not be used in production. You should instead set "_serialize"
		// in each action as required.
		if (!array_key_exists('_serialize', $this->viewVars) &&
			in_array($this->response->type(), ['application/json', 'application/xml'])
		) {
			$this->set('_serialize', true);
		}
		

		$this->request->params['loginUser'] = [];

		if($this->Auth !== false){
			$id = $this->Auth->user('id');

			if($id){
				switch($this->request->prefix){
					case 'agency' : 
						$loginUser = $this->Agencies->find('detail', ['options' => ['id' => $id]])->toArray();
					break;
					case 'mypage' : 
						$loginUser = $this->Users->find('all')->where(['Users.id' => $id])->contain(['Agencies'])->toArray();
					break;
				}

				if(!empty($loginUser)) $this->request->params['loginUser'] = $loginUser[0];
			}
		}

	}

	protected function setPolicy($policy = ''){
		$this->set('policy', $policy);
	}


	protected function setTitle($title = ''){
		$this->set('title', $title);
	}

	protected function setSubTitle($subtitle = ''){
		$this->set('subtitle', $subtitle);
	}

	protected function saveUserAttachmentValue($userId){

		if(empty($this->request->data['form'])){
			return true;
		}

		$datas = [];
		foreach($this->request->data['form'] as $k => $v){
			$datas[] = [
				'user_id' => $userId,
				'form_code' => $k,
				'value' => serialize($v)
			];
		}
		$entities = $this->FormValues->newEntities($datas);
		$this->FormValues->deleteAll(['user_id' => $userId]);
		foreach($entities as $entity){
			$this->FormValues->save($entity);
		}

		return true;
	}

	protected function loadUserAttachmentValue($userId){

		$formValues = $this->FormValues->find()->where(['user_id' => $userId])->toArray();
		$return = [];
		foreach($formValues as $v){
			$return[$v['form_code']] = unserialize($v['value']);
		}

		return $return;
	}

	protected function setUserAttachmentForm($agencyId){

		$formParams = $this->Forms->find('formParams',['options' => ['agency_id' => $agencyId]]);
		$this->set('formParams', $formParams);
	}


    public function file_upload ($file = null, $dir = null, $limitFileSize = 1024 * 1024){

        try {
            // ファイルを保存するフォルダ $dirの値のチェック
            if ($dir){
                if(!file_exists($dir)){
	            	mkdir($dir, 0777, true);
                }
            } else {
                throw new RuntimeException('ディレクトリの指定がありません。');
            }
 
            // 未定義、複数ファイル、破損攻撃のいずれかの場合は無効処理
            if (!isset($file['error']) || is_array($file['error'])){
                throw new RuntimeException('Invalid parameters.');
            }
 
            // エラーのチェック
            switch ($file['error']) {
                case 0:
                    break;
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }
 
            // ファイル情報取得
            $fileInfo = new File($file["tmp_name"]);
 
            // ファイルサイズのチェック
            if ($fileInfo->size() > $limitFileSize) {
                throw new RuntimeException('Exceeded filesize limit.');
            }
 
            // ファイルタイプのチェックし、拡張子を取得

            if (false === $ext = array_search($fileInfo->mime(), $this->allowedFileFormat, true)){
                throw new RuntimeException('Invalid file format.');
            }

			$fileParam = explode('.', $file['name']);
 
            // ファイル名の生成
            $uploadFile = sha1(date('YmdHis') . rand(10000,99999)) . "." . end($fileParam);
 
            // ファイルの移動
            if (!@move_uploaded_file($file["tmp_name"], $dir . "/" . $uploadFile)){
                throw new RuntimeException('Failed to move uploaded file.');
            }
 
        } catch (RuntimeException $e) {
            throw $e;
        }
        return $uploadFile;
    }

	/**
	 * 
	 * @param type $uploadfile
	 * @param string $from_encoding 変換前の文字エンコーディング名(既定は'auto')
	 * @return type
	 */
	public static function importCSV($uploadfile, $from_encoding = 'auto') {
		$records = [];
		

		if (($fp = fopen($uploadfile, "r")) === false) {
			return [];
		}

		// CSVの中身がダブルクオーテーションで囲われていない場合に一文字目が化けるのを回避
		// setlocale(LC_ALL, 'ja_JP');

		$i=0;

		while (($line = fgetcsv($fp)) !== FALSE) {
			mb_convert_variables('UTF-8', 'sjis-win', $line);
			p($line);
			if($i == 0){
				// タイトル行
				$header = $line;
				$i++;
				continue;
			}

			$records[] = $line;

			$i++;
		}

		fclose($fp);

		return $records;
	}

}
