<?php 
namespace App\Controller;

use Cake\Controller\Controller;
use App\Controller\AppController;

class MypageController extends AppController
{
	public function index(){
		$this->redirect('/mypage/profiles');
	}
}