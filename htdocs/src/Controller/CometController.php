<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Time;
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CometController extends AppController
{


	public function initialize()
	{
		parent::initialize();
	}

	public function beforeFilter(Event $event)
	{
		$this->autoRender = false;
		parent::beforeFilter($event);
	}

	// 1回のコネクションごとに1回のキーを発行する
	public function getToken(){
		$token = hash('SHA256', date('YmdHis') . mt_rand(10000, 99999)) . '_' . date('Y-m-d H:i:s');
		$this->Session->write('Comet.token', $token);
		echo $token;
		exit;
	}

	public function getMessage(){

		$loggedToken = $this->Session->read('Comet.token');
		$postedToken = $this->request->data('token');
		$dateOfToken = explode('_', $loggedToken)[1];

		if($loggedToken != $postedToken){
			echo 'bad request';
			exit;
		}

		$loginUser = [];
		$loginType = '';

		if(!empty($this->Session->read('Mypage')) && strpos($_SERVER['HTTP_REFERER'],'/mypage/')){
			$loginUser = $this->Session->read('Mypage');
			$loginType = 'user';
		}elseif(!empty($this->Session->read('Agency')) && strpos($_SERVER['HTTP_REFERER'],'/agency/')){
			$loginUser = $this->Session->read('Agency');
			$loginType = 'agency';
		}

		$returnFlg = false;
		$count = 0;

		$userId = $loginUser['id'];

		$this->response->header('Content-type', 'application/octet-stream');
		$this->response->header('Transfer-encoding', 'chunked');
		flush();

		while(!$returnFlg){

			$rooms = $this->MessageRoomUsers->find('memberRoomList', ['options' => ['member_id' => $userId, 'member_type' => $loginType]])->toArray();

			foreach($rooms as $room){
				$modified = $room['message_room']->modified;
				if(strtotime($modified) > strtotime($dateOfToken)){
					echo 'test';
					exit;
				}
			}

			flush();
			sleep(1);
			$count++;
			if(5 <= $count){
				$returnFlg = true;
			}
		}

		exit;
	}
}
