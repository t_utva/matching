<?php 
namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MatchingsTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->belongsTo('Users', [
			'foreignKey' => 'applicant_user_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Users', [
			'foreignKey' => 'passive_user_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Agencies', [
			'foreignKey' => 'applicant_user_agency_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Agencies', [
			'foreignKey' => 'passive_user_agency_id',
			'joinType' => 'INNER'
		]);
	}

	/**
	 * /api/matchingsの一覧を返す
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findIndexList(Query $query, array $options) {

		$options = $options['options'];

		if(!empty($options['applicant_user_id'])){
			$query->where(['applicant_user_id' => $options['applicant_user_id']]);
		}

		if(!empty($options['applicant_user_agency_id'])){
			$query->where(['applicant_user_agency_id' => $options['applicant_user_agency_id']]);
		}

		$query->order(['Matchings.modified' => 'DESC'])->contain(['Users', 'Agencies']);

		return $query;
	}

	public function findDetail(Query $query, array $options) {
		$options = $options['options'];
		$query->where(['matching_code' => $options['matching_code']]);
		return $query;
	}

}