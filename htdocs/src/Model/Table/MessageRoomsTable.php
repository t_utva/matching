<?php 
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessageRoomsTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		//parent::initialize($config);

		$this->addBehavior('Timestamp', [
			'events' => [
				'Model.beforeSave' => [
					'modified' => 'always'
				],
			]
		]);



		$this->hasMany('MessageRoomUsers', [
			'foreignKey' => 'message_room_id',
		]);

		$this->hasMany('MessageContents', [
			'foreignKey' => 'message_room_id',
		]);

	}



	/**
	 * userId、userTypeに紐づくルームリストおよび、カクルールムに紐づくユーザーの一覧を取得する
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findMemberRoomList(Query $query, array $options) {
		// 条件を取り出し
		$options = $options['options'];

		$query->leftJoinwith('MessageRoomUsers', function ($q) use($options) {
			$q->where([
				'MessageRoomUsers.member_id'   => $options['id'],
				'MessageRoomUsers.member_type' => $options['type']
			]);

			return $q;
		});


		return $query;
	}

	
}