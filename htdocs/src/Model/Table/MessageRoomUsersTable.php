<?php 
namespace App\Model\Table;

use App\Model\Entity\User;
use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessageRoomUsersTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);
		
		$this->belongsTo('MessageRooms', [
			'foreignKey' => 'message_room_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Users', [
			'foreignKey' => 'member_id',
		]);

		$this->belongsTo('Agencies', [
			'foreignKey' => 'member_id',
		]);

		$this->hasOne('MessageContents', [
			'foreignKey' => 'message_room_user_id'
		]);

	}


	/**
	 * userId、userTypeに紐づくroomListおよび、かくroomに所属するユーザーリストの取得
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findMemberRoomList(Query $query, array $options) {
		// 条件を取り出し
		$options = $options['options'];

		$query->where([
			'member_id'   => $options['member_id'],
			'member_type' => $options['member_type']
		]);

		$query->contain(['MessageRooms' => function($q){
			$q->contain(['MessageRoomUsers']);
			$q->contain(['MessageContents']);
			return $q;
		}]);

		return $query;
	}



}