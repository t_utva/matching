<?php 
namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FormsTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->belongsTo('Agencies', [
			'foreignKey' => 'agency_id',
			'joinType' => 'INNER'
		]);
	}


	/**
	 * 業者毎の会員追加項目一覧を取得する
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findFormParams(Query $query, array $options) {
		// 条件を取り出し
		$options = $options['options'];

		$query->where(['agency_id' => $options['agency_id']]);

		return $query;
	}


}