<?php 
namespace App\Model\Table;

use App\Model\Entity\User;
use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessageContentsTable extends AppTable
{
	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);
		
		$this->hasMany('MessageContentRecipients', [
			'foreignKey' => 'message_content_id',
		]);

		$this->belongsTo('MessageRooms', [
			'foreignKey' => 'message_room_id',
			'joinType' => 'INNER'
		]);

		$this->hasOne('MessageRoomUsers', [
			'foreignKey' => 'id'
		]);
	}

	/**
	 * message_room_idに紐づくメッセージを取得
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findMessageContent(Query $query, array $options){
		// 条件を取り出し
		$options = $options['options'];


		$query->where([
			'MessageContents.message_room_id'   => $options['message_room_id']
		]);

		$query->contain('MessageRoomUsers' , function ($q) use($options){

			$q->where([
				'MessageRoomUsers.member_type' => $options['member_type']
			]);

			if($options['member_type'] == 'user'){
				$q->contain('Users');
			}else{
				$q->contain('Agencies');
			}

			return $q;
		});


		$query->limit($options['limit']);

		return $query;
	}

}