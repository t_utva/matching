<?php 
namespace App\Model\Table;
use App\Model\Entity\User;
use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->belongsTo('Agencies', [
			'foreignKey' => 'agency_id',
			'joinType' => 'INNER'
		]);
		$this->hasMany('Matchings', [
			'foreignKey' => 'applicant_user_id',
		]);
		$this->hasMany('Matchings', [
			'foreignKey' => 'passive_user_id',
		]);

		$this->hasMany('MessageRoomUsers', [
			'foreignKey' => 'member_id',
		]);



	}


	/**
	 * /api/usersの一覧を返す
	 * @param \Cake\ORM\Query $query クエリオブジェクト
	 * @param array $options パラメタ
	 * @return \Cake\ORM\Query 作成したクエリオブジェクト
	 */
	public function findIndexList(Query $query, array $options) {
		// 条件を取り出し
		$options = $options['options'];

		$query->where(['id <>' => $options['userModel']['id']]);
		$query->where(['agency_id' => $options['userModel']['agency_id']]);

		// 検索条件
		if (!empty($options['sex'])) $query->where(['sex' => $options['sex']]);
		//if (!empty($options['desired_age']))
		if (!empty($options['desired_salary'])){
			if($options['desired_salary'] == 1){
				$query->where(['income <' => '2000000']);
			}elseif($options['desired_salary'] == 10){
				$query->where(['income >=' => '10000000']);
			}else{
				$query->where(['income >=' => $options['desired_salary'] * 1000000]);
				$query->where(['income <' => ($options['desired_salary'] + 1) * 1000000]);
			}
		}
		if (!empty($options['desired_pref'])) $query->where(['pref' => $options['desired_pref']]);
		if (!empty($options['desired_job'])) $query->where(['job in ' => $options['desired_job']]);

		/*
		// 検索条件を付加
		$query->where(['client_id' => $options['clientId']]);
		if (isset($options['authority'])) $query->where(['authority in' => $options['authority']]);
		if (isset($options['status'])) $query->where(['status' => $options['status']]);

		// ソート順を付加
		$query->order([$options['sort'] => $options['order']]);

		if($options['sort'] != 'id'){
			$query->order(['id' => 'ASC']);
		}        

		// 取得件数を付加
		if (isset($options['limit']) && (isset($options['offset']))) {
			$query->offset($options['offset'])->limit($options['limit']);
		}

		// 取得フィールドを宣言
		$query->select(['id', 'client_id', 'name', 'kana', 'code', 'authority', 'status']);
		*/
		return $query;
	}


	public function findDetail(Query $query, array $options) {
		$options = $options['options'];
		if(!empty($options['user_code'])) $query->where(['user_code' => $options['user_code']]);
		if(!empty($options['id'])) $query->where(['Users.id' => $options['id']]);
		$query->contain(['Agencies']);

		return $query;
	}

}