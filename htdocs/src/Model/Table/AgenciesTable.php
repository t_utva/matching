<?php 
namespace App\Model\Table;

use App\Model\Entity\Agency;
use App\Model\Table\AppTable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AgenciesTable extends AppTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->hasMany('Users', [
			'foreignKey' => 'agency_id',
		]);
		$this->hasMany('Forms', [
			'foreignKey' => 'agency_id',
		]);
		$this->hasMany('UserCsvs', [
			'foreignKey' => 'agency_id',
		]);
		$this->hasMany('Matchings', [
			'foreignKey' => 'applicant_user_agency_id',
		]);
		$this->hasMany('Matchings', [
			'foreignKey' => 'passive_user_agency_id',
		]);

	}


	public function findDetail(Query $query, array $options) {
		$options = $options['options'];
		if(!empty($options['id'])) $query->where(['Agencies.id' => $options['id']]);

		return $query;
	}



}