<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property int $client_id
 * @property \App\Model\Entity\Client $client
 * @property string $name
 * @property string $kana
 * @property string $code
 * @property string $password
 * @property int $authority
 * @property bool $status
 * @property \Cake\I18n\Time $created
 * @property int $created_user
 * @property \Cake\I18n\Time $modified
 * @property int $modified_user
 * @property \App\Model\Entity\CallHistory[] $call_histories
 * @property \App\Model\Entity\CallHistoriesLog[] $call_histories_logs
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\NotificationsLog[] $notifications_logs
 */
class User extends Entity
{


    /**
     * パスワードのhashを保存する
     */
    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

}
