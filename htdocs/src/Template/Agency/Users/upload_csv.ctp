<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">

								<div class="x_panel">
									<div class="x_title">
										<h2><?=$subtitle?></h2>
										<div class="clearfix"></div>
									</div>


									<div class="x_content">

										<?=$this->Form->create('', ['type' => 'post', 'url' => '/agency/users/upload_csv_confirm', 'enctype' => 'multipart/form-data' ,'data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>

										<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">CSVファイル</label></div>
										<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
											<div class="form-group">
												<?=$this->Form->file('upload', ['class' => 'form-control'])?>
											</div>
										</div>

										<div class="clearfix"></div>

										<div class="ln_solid"></div>


										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<?=$this->Form->button('登録する', ['type' => 'submit','class' => 'btn btn-success']) ?>
											</div>
										</div>

										<div class="clearfix"></div>

										<div class="ln_solid"></div>

										<h4>申請状況</h4>

										<table class="table table-striped projects">
										<tr>
											<th>申請日</th>
											<th>ファイル</th>
											<th>ステータス</th>
											<th>削除</th>
										</tr>
										<?php foreach($datas as $data): ?>
										<tr>
											<td><?=$data->created->format('Y年m月d日 H時i分m秒')?></td>
											<td><a href="/upload/upload_csv/<?=$data->file?>"><?=$data->file?></a></td>
											<td><?=Configure::read('csv')[$data->status]?></td>
											<td><a href="/agency/users/delete_csv/<?=$data->id?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> 削除 </a></td>
										</tr>
										<?php endforeach; ?>
										</table>

										<?=$this->Form->end() ?>

									</div>

								</div>
							</div>
						</div>
