<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">

								<div class="x_panel">
									<div class="x_title">
										<h2><?=$subtitle?></h2>
										<div class="clearfix"></div>
									</div>


									<div class="x_content">

										<?=$this->Form->create('', ['type' => 'post', 'url' => '/agency/users/upload_csv_complete', 'data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>

										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button type="submit" class="btn btn-success">登録する</button>
											</div>
										</div>

										<div class="clearfix"></div>

										<div class="ln_solid"></div>

										<!-- start project list -->
										<table class="table table-striped projects">
											<?php foreach($dataset as $k => $datas):?>
											<tr>
											<?php if($k == 0):?>
												<?php 
												foreach($datas as $data):
													$data = str_replace(["\r\n", "\r", "\n"], '', $data);
													$data = preg_replace('/（(.*)）/i', '', $data);
													$data = preg_replace('/\((.*)\)/i', '', $data);
												?>
												<th><?=$data?></th>
												<?php endforeach; ?>
											<?php else: ?>
												<?php foreach($datas as $data): ?>
												<td><?=$data?></td>
												<?php endforeach; ?>
											<?php endif; ?>
											</tr>
											<?php endforeach; ?>
										</table>
										<!-- end project list -->

										<div class="ln_solid"></div>

										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
												<button type="submit" class="btn btn-success">登録する</button>
											</div>
										</div>

										<?=$this->Form->end() ?>

									</div>

								</div>
							</div>
						</div>
