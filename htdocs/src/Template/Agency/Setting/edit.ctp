<?php
/**
* CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
* @link          https://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       https://opensource.org/licenses/mit-license.php MIT License
*/

// 開発メモ 将来的に https://www.ibjapan.com/area/fukuoka/ みたいな一覧と詳細をもたせられればと。
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$subtitle?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<?=$this->element('agency_profile_form');?>
			</div>
		</div>
	</div>
</div>