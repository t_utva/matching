<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<?=$this->Html->css(['/css/message.css']);?>
<div class="row">

	<div id="frame">
		<div id="sidepanel">
			<?php /* 
			<div id="profile">
				<div class="wrap">
					<img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="" />
					<p>Mike Ross</p>
					<i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>
					<div id="status-options">
						<ul>
							<li id="status-online" class="active"><span class="status-circle"></span> <p>Online</p></li>
							<li id="status-away"><span class="status-circle"></span> <p>Away</p></li>
							<li id="status-busy"><span class="status-circle"></span> <p>Busy</p></li>
							<li id="status-offline"><span class="status-circle"></span> <p>Offline</p></li>
						</ul>
					</div>
					<div id="expanded">
						<label for="twitter"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="mikeross" />
						<label for="twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="ross81" />
						<label for="twitter"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></label>
						<input name="twitter" type="text" value="mike.ross" />
					</div>
				</div>
			</div>
			<div id="search">
				<label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
				<input type="text" placeholder="Search contacts..." />
			</div>
			*/ ?>
			<div id="contacts">
				<ul>
					<?php foreach($rooms as $room): ?>
					<li class="contact<?=($room->name == $roomName)? ' active': ''; ?>" onclick="javascript:location.href='/agency/messages/message/<?=$room->name?>/'">
						<div class="wrap">
							<?php $image = (!empty($room->userInfo[0]->image))? $room->userInfo[0]->image : '/images/user.png' ?>
							<span class="contact-status online"></span>
							<img src="<?=$image?>" alt="" />
							<div class="meta">
								<p class="name"><?=$this->Matching->getMessageUserName($room->userInfo[0])?></p>
								<p class="preview"><?=!empty(end($room->message_contents)->body)? end($room->message_contents)->body: 'メッセージがありません'?></p>
							</div>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php /*
			<div id="bottom-bar">
				<button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add contact</span></button>
				<button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
			</div>
			*/ ?>
		</div>
		<div class="content">

			<div class="contact-profile">
				<?php foreach($roomUsers as $user): ?>
				<img src="<?=(!empty($user->image))? $user->image : '/images/user.png' ?>" alt="" />
				<p><?=(!empty($user->agency_code))? $user['name'] : $user['firstname'] . $user['lastname']; ?></p>
				<?php endforeach; ?>
				<?php /*
				<div class="social-media">
					<i class="fa fa-facebook" aria-hidden="true"></i>
					<i class="fa fa-twitter" aria-hidden="true"></i>
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</div>
				*/ ?>
			</div>
			<div class="messages">
				<ul>
					<?php foreach($contents as $content): ?>
					<?php $cls = ($content->message_room_user_id == $userId)? 'sent' : 'replies';?>
					<li class="<?=$cls?>">
						<img src="<?=(!empty($content->message_room_user->user->image))? $content->message_room_user->user->image : '/images/user.png' ?>" alt="" />
						<p><?=nl2br($content->body)?></p>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="message-input">
				<div class="wrap">
				<input type="text" class="message-body" placeholder="Write your message..." />
				<input type="hidden" class="message-room" value="<?=$roomName?>">
				<!--i class="fa fa-paperclip attachment" aria-hidden="true"></i-->
				<button class="submit"><i class="fa fa-send-o" aria-hidden="true"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>