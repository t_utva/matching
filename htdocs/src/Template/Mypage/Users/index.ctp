<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$subtitle?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">



				<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
					<div class="profile_img">
						<div id="crop-avatar">
							<!-- Current avatar -->
							<img class="img-responsive avatar-view" src="/images/picture.jpg" alt="Avatar" title="Change the avatar">
						</div>
					</div>
					<h3>福岡 太郎</h3>

					<ul class="list-unstyled user_data">
						<li><i class="fa fa-heart user-profile-icon"></i> 1980年4月15日生
						</li>

						<li>
							<i class="fa fa-group user-profile-icon"></i> エフコネクト
						</li>

						<li class="m-top-xs">
							<i class="fa fa-facebook-square user-profile-icon"></i>
							<a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
						</li>

						<li class="m-top-xs">
							<i class="fa fa-twitter-square user-profile-icon"></i>
							<a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
						</li>

						<li class="m-top-xs">
							<i class="fa fa-instagram user-profile-icon"></i>
							<a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
						</li>
					</ul>

					<a class="btn btn-success"><i class="fa fa-star m-right-xs"></i>お気に入り登録</a><br />
					<a class="btn btn-default"><i class="fa fa-check-square m-right-xs"></i>お見合い申込</a><br />
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">

					<h2>基本プロフィール</h2>
					<table class="data table table-striped no-margin">
						<tbody>
							<tr>
								<th>年齢／生年月</th>
								<td>40歳／1975年6月</td>
								<th>お住まい</th>
								<td>福岡県／福岡市中央区</td>
							</tr>
							<tr>
								<th>身長／体重／血液型</th>
								<td>170cm／50kg〜54kg／A型</td>
								<th>続柄</th>
								<td>長男</td>
							</tr>
							<tr>
								<th>最終学歴</th>
								<td>短期大学卒業（非公開）</td>
								<th>年収</th>
								<td>非公開</td>
							</tr>
							<tr>
								<th>職業</th>
								<td>会社員</td>
								<th>勤務地／職業備考</th>
								<td>福岡市中央区</td>
							</tr>
						</tbody>
					</table>


					<h2>趣味／趣向</h2>

					 <table class="data table table-striped no-margin">
						<tbody>
							<tr>
								<th>出身国／国籍</th>
								<td>福岡県／日本</td>
								<th>婚歴</th>
								<td>初婚</td>
							</tr>
							<tr>
								<th>離婚理由</th>
								<td>なし</td>
								<th>子供</th>
								<td>なし</td>
							</tr>
							<tr>
								<th>趣味</th>
								<td>ドライブ</td>
								<th>資格</th>
								<td>普通自動車免許</td>
							</tr>
							<tr>
								<th>本人資産</th>
								<td>なし</td>
								<th>その他資産</th>
								<td>土地・家屋（親名義）</td>
							</tr>
							<tr>
								<th>お酒／たばこ</th>
								<td>付き合い程度／吸わない</td>
								<th>宗教／宗教名</th>
								<td>なし</td>
							</tr>
						</tbody>
					</table>

					<h2>お相手への希望</h2>

					<table class="data table table-striped no-margin">
						<tbody>
							<tr>
								<th>お相手への希望</th>
								<td>誠実で思いやりのある方。<br>お人柄を重視します。</td>
								<th>希望年齢</th>
								<td>なし</td>
							</tr>
							<tr>
								<th>希望年収</th>
								<td>なし</td>
								<th>希望地域</th>
								<td>福岡県</td>
							</tr>
							<tr>
								<th>相手家族同居</th>
								<td>難しい</td>
								<th>自分家族同居</th>
								<td>希望しない</td>
							</tr>
							<tr>
								<th>結婚後子供</th>
								<td>欲しい</td>
								<th>養子について</th>
								<td>希望しない</td>
							</tr>
							<tr>
								<th>再婚者</th>
								<td>相手による</td>
								<th>&nbsp;</th>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>

					<h2 class="page-header">PR／コメント</h2>

					<h4>自己PR</h4>
					<p>落ち着きはありますが、朗らかで結構芯が強いと思います。ドライブが好きで休日には新しいドライブスポットを巡るのが楽しいです。</p>
					<h4>この方の担当カウンセラーからのPR</h4>
					<p>すらっとした、明るく性格の良い男性です。お料理が得意で落ち着いた物腰、美術に造詣が深く知性があふれていますが、決して驕らない優しい気持ちを持った男性です。</p>

				</div>


			</div>
		</div>
	</div>
</div>
