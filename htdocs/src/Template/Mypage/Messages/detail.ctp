<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$subtitle?></h2>

                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div>
                        <!-- end of user messages -->
                        <ul class="messages">
                          <li>
                            <img src="/images/picture.jpg" class="avatar" alt="Avatar">
                            <div class="message_date">
                              <h3 class="date text-info">24</h3>
                              <p class="month">May</p>
                            </div>
                            <div class="message_wrapper">
                              <h4 class="heading">福岡 太郎</h4>
                              <blockquote class="message">こんにちは！</blockquote>
                              <br />
                            </div>
                          </li>
                          <li>
                            <img src="/images/img.jpg" class="avatar" alt="Avatar">
                            <div class="message_date">
                              <h3 class="date text-error">21</h3>
                              <p class="month">May</p>
                            </div>
                            <div class="message_wrapper">
                              <h4 class="heading">福岡 花子</h4>
                              <blockquote class="message">こんにちは、お元気ですか！？</blockquote>
                              <br />
                            </div>
                          </li>
                          <li>
                            <img src="/images/picture.jpg" class="avatar" alt="Avatar">
                            <div class="message_date">
                              <h3 class="date text-info">24</h3>
                              <p class="month">May</p>
                            </div>
                            <div class="message_wrapper">
                              <h4 class="heading">福岡 花子</h4>
                              <blockquote class="message">元気です！太郎さんはお元気ですか？？</blockquote>
                              <br />
                            </div>
                          </li>
                        </ul>
                        <!-- end of user messages -->


                      </div>


                    <!-- start form for validation -->
                    <form id="demo-form" data-parsley-validate>
                      <label for="message">メッセージ :</label>
                      <textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10"></textarea>

                      <br/>
                      <span class="btn btn-primary">送信する</span>
                    </form>
                    <!-- end form for validations -->



                    </div>

                    <!-- start project-detail sidebar -->
                    <div class="col-md-3 col-sm-3 col-xs-12">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="/images/picture.jpg" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>福岡 太郎</h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-heart user-profile-icon"></i> 1980年4月15日生
                        </li>

                        <li>
                          <i class="fa fa-group user-profile-icon"></i> エフコネクト
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-facebook-square user-profile-icon"></i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-twitter-square user-profile-icon"></i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-instagram user-profile-icon"></i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                        </li>
                      </ul>

                      <a class="btn btn-success"><i class="fa fa-star m-right-xs"></i>お気に入り登録</a><br>
                      <a class="btn btn-default"><i class="fa fa-check-square m-right-xs"></i>お見合い申込</a><br>
                    </div>

                  </div>
                </div>
              </div>
            </div>