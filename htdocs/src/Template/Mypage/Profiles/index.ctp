<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright  	  Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$subtitle?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<?=$this->element('mypage_usercard',['user' => $user])?>

				<div class="col-md-9 col-sm-9 col-xs-12">
					<!--
					<div class="profile_title">
						<div class="col-md-6"><h2>プロフィール閲覧回数</h2></div>
						<div class="col-md-6">
							<div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
								<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
								<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
							</div>
						</div>
					</div>
					-->
					<!-- start of user-activity-graph -->
					<!--div id="graph_bar" style="width:100%; height:280px;"></div-->
					<!-- end of user-activity-graph -->

					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">基本プロフィール</a></li>
						<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">趣味／趣向</a></li>
						<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">お相手への希望</a></li>
						<li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">PR／コメント</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

								<table class="data table table-striped no-margin">
								<tbody>
								<tr>
									<th>年齢／生年月</th>
									<td>
										<?php $date = new Date($user->birthday); ?>
										<?=(int)((date('Ymd')-$date->format('Ymd'))/10000);?>歳／<?=$date->format('Y年m月');?>
									</td>
									<th>お住まい</th>
									<td><?=Configure::read('pref')[$user->live_pref]?>／<?=$user->live_city?></td>
								</tr>
								<tr>
									<th>身長／体重／血液型</th>
									<td><?=$user->height?>cm／<?=$user->weight?>kg／<?=Configure::read('blood_type')[$user->blood_type]?></td>
									<th>続柄</th>
									<td><?=Configure::read('relationship')[$user->relationship]?></td>
								</tr>
								<tr>
									<th>最終学歴</th>
									<td><?=Configure::read('edu_back')[$user->edu_back]?>（<?=Configure::read('edu_back_type')[$user->edu_back_type]?>）</td>
									<th>年収</th>
									<td><?=$user->income?>円</td>
								</tr>
								<tr>
									<th>職業</th>
									<td>-</td>
									<th>勤務地／職業備考</th>
									<td><?=Configure::read('pref')[$user->work_pref]?><?=$user->work_city?>／<?=$user->work_remarks?></td>
								</tr>
								</tbody>
								</table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
								<table class="data table table-striped no-margin">
								<tbody>
								<tr>
									<th>出身国／国籍</th>
									<td><?=$user->nationality?></td>
									<th>婚歴</th>
									<td><?=Configure::read('history')[$user->history]?></td>
								</tr>
								<tr>
									<th>離婚理由</th>
									<td><?=$user->reason?></td>
									<th>子供</th>
									<td><?=Configure::read('children')[$user->children]?></td>
								</tr>
								<tr>
									<th>趣味</th>
									<td><?=$user->hobby?></td>
									<th>資格</th>
									<td><?=$user->qualification?></td>
								</tr>
								<tr>
									<th>本人資産</th>
									<td><?=$user->asset?></td>
									<th>その他資産</th>
									<td><?=$user->asset_other?></td>
								</tr>
								<tr>
									<th>お酒／たばこ</th>
									<td><?=Configure::read('alcohol')[$user->alcohol]?>／<?=Configure::read('tobacco')[$user->tobacco]?></td>
									<th>宗教／宗教名</th>
									<td><?=Configure::read('exist')[$user->religion]?>／<?=$user->religion_name?></td>
								</tr>
								</tbody>
								</table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
								<table class="data table table-striped no-margin">
								<tbody>
								<tr>
									<th>お相手への希望</th>
									<td><?=$user->desired_message?></td>
									<th>希望年齢</th>
									<td>
										<?=Configure::read('age_option')[$user->desired_age_low]?>
										<?php if($user->desired_age_low != $user->desired_age_high): ?>〜<?=Configure::read('age_option')[$user->desired_age_high]?><?php endif; ?>
									</td>
								</tr>
								<tr>
									<th>希望年収</th>
									<td>
										<?=Configure::read('income_option')[$user->desired_salary_low]?>
										<?php if($user->desired_salary_low != $user->desired_salary_high): ?>〜<?=Configure::read('income_option')[$user->desired_salary_high]?><?php endif; ?>
									</td>
									<th>希望地域</th>
									<td><?=Configure::read('pref')[$user->desired_pref]?><?=$user->desired_city?></td>
								</tr>
								<tr>
									<th>相手家族同居</th>
									<td><?=Configure::read('prop')[$user->desired_family01]?></td>
									<th>自分家族同居</th>
									<td><?=Configure::read('desired_family02')[$user->desired_family02]?></td>
								</tr>
								<tr>
									<th>結婚後子供</th>
									<td><?=Configure::read('hope')[$user->about_child]?></td>
									<th>養子について</th>
									<td><?=Configure::read('hope_b')[$user->about_adopted]?></td>
								</tr>
								<tr>
									<th>再婚者</th>
									<td><?=Configure::read('prop')[$user->about_remarrior]?></td>
									<th>&nbsp;</th>
									<td>&nbsp;</td>
								</tr>
								</tbody>
								</table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
								<h4>自己PR</h4>
								<p><?=$user->pr01 ?></p>
								<h4>この方の担当カウンセラーからのPR</h4>
								<p><?=$user->pr02 ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
