<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$subtitle?></h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

				<div class="well">
					残りお見合い申し込み回数：○回
				</div>


				<?=$this->Form->create(null,['url' => '/mypage/matching/apply_confirm','data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>
				<?=$this->Form->hidden('user_code', ['value' => $this->request->params['pass'][0]])?>
				<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">お相手へのメッセージ</label></div>
				<div class="col-md-10 col-sm-12 col-xs-12">
					<div class="form-group">
						<?=$this->Form->textarea('message', ['class' => 'form-control']) ?>
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<?=$this->Form->button('登録する', ['type' => 'submit','class' => 'btn btn-success']) ?>
					</div>
				</div>
				<?=$this->Form->end() ?>
			</div>
		</div>
	</div>
</div>
