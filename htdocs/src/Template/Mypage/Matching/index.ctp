<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$subtitle?></h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

				<div class="well">
					残りお見合い申し込み回数：○回
				</div>

				<!-- start project list -->
				<table class="table table-striped projects">
					<thead>
						<tr>
							<th style="width: 1%">id</th>
							<th>写真</th>
							<th>会員名</th>
							<th>申込日</th>
							<th>ステータス</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($matchings as $matching): ?>
						<?=$this->element('matching_row', ['policy' => $policy, 'matching' => $matching])?>
						<?php endforeach; ?>
				</table>
				<!-- end project list -->
			</div>
		</div>
	</div>
</div>
