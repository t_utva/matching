<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// 開発メモ 将来的に https://www.ibjapan.com/area/fukuoka/ みたいな一覧と詳細をもたせられればと。
?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$subtitle?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="./result" method="post" data-parsley-validate class="form-horizontal form-label-left">

                      <h4>会員向け設定</h4>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">他団体連携</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                        <div class="radio">
                          <label><input type="radio">&nbsp;許可しない</label>&nbsp;
                          <label><input type="radio">&nbsp;許可する</label><br>
                          <small>* 連携許可すると、許可団体間で会員の交流が可能となります。</small>
                        </div>
                      </div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">会員直連絡</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group">
                        <div class="radio">
                          <label><input type="radio">&nbsp;許可しない</label>&nbsp;
                          <label><input type="radio">&nbsp;許可する</label><br>
                          <small>* 許可すると会員同士が直接連絡可能となります。許可しない場合は全てのメッセージが管理者宛に送信されます。</small>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">登録する</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>

              </div>
            </div>

