<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// 開発メモ 将来的に https://www.ibjapan.com/area/fukuoka/ みたいな一覧と詳細をもたせられればと。
?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                  <div class="x_title">
                    <h2><?=$subtitle?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" action="./result" method="post" data-parsley-validate class="form-horizontal form-label-left">


                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">社名</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group"><input type="text" class="form-control"></div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">所在地</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group"><input type="text" class="form-control"></div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">電話番号</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group"><input type="text" class="form-control"></div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">URL</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group"><input type="text" class="form-control"></div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">PR</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <textarea id="message" class="form-control" name="message"></textarea>
                        </div>
                      </div>

                      <div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">画像</label></div>
                      <div class="col-md-10 col-sm-12 col-xs-12 form-group"><input type="file" class="form-control"></div>

                      <div class="clearfix"></div>

                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">登録する</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>

              </div>
            </div>

