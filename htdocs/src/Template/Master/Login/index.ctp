<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
					<section class="login_content">
						<?= $this->Form->create() ?>
							<h1> <?=$title?> </h1>
							<?= $this->Flash->render() ?>
							<div>
								<?= $this->Form->text('login_id',['class' => 'form-control', 'placeholder' => 'ログインID', 'required' => true]) ?>
							</div>
							<div>
								<?= $this->Form->control('password',['type' => 'password', 'label' => false, 'class' => 'form-control', 'placeholder' => 'パスワード', 'required' => true]) ?>
							</div>
							<div>
								<button class="btn btn-default submit">ログイン</button>
								<a class="reset_pass" href="#">パスワードを忘れた方</a>
							</div>

							<div class="clearfix"></div>
						</form>
					</section>
