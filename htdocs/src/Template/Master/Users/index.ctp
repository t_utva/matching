<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">

								<div class="x_panel">
									<div class="x_title">
										<h2>検索</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content" style="display: none;">
										<br />
										<?=$this->element('member_search')?>
									</div>
								</div>

								<div class="x_panel">
									<div class="x_title">
										<h2><?=$subtitle?></h2>
										<div class="clearfix"></div>
									</div>


									<div class="x_content">

										<?=$this->element('pagination');?>

										<!-- start project list -->
										<table class="table table-striped projects">
											<thead>
												<tr>
													<th style="width: 1%">id</th>
													<th>写真</th>
													<th>団体名</th>
													<th>会員名</th>
													<th>性別</th>
													<th>年齢</th>
													<th>基礎情報</th>
													<th>職業</th>
													<th>年収</th>
													<th>地域</th>
													<th>最終ログイン日時</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($users as $user): ?>
												<?=$this->element('member_row', ['policy' => $policy, 'user' => $user])?>
												<?php endforeach; ?>
										</table>
										<!-- end project list -->

										<?=$this->element('pagination');?>

									</div>

								</div>
							</div>
						</div>
