<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">

								<div class="x_panel">
									<div class="x_title">
										<h2><?=$subtitle?></h2>
										<div class="clearfix"></div>
									</div>


									<div class="x_content">

										<table class="table table-striped projects">
										<tr>
											<th>業者名</th>
											<th>申請日</th>
											<th>ファイル</th>
											<th>ステータス</th>
											<th>操作</th>
										</tr>
										<?php foreach($datas as $data): ?>
										<tr>
											<td><?=$data->agency->name?></td>
											<td><?=$data->created->format('Y年m月d日 H時i分m秒')?></td>
											<td><a href="/upload/upload_csv/<?=$data->file?>"><?=$data->file?></a></td>
											<td><?=Configure::read('csv')[$data->status]?></td>
											<td>
												<?php if($data->status == 0): ?>
												<a href="/master/users/import_complete/approve/<?=$data->id?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> 承認 </a>
												<a href="/master/users/import_complete/dismissal/<?=$data->id?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> 却下 </a>
												<?php endif; ?>
											</td>
										</tr>
										<?php endforeach; ?>
										</table>


									</div>

								</div>
							</div>
						</div>
