<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">

				<div class="x_panel">
					<div class="x_title">
						<h2><?=$subtitle?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<?=$this->Form->create($form,['data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>

							<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">表示名</label></div>
							<div class="col-md-10 col-sm-12 col-xs-12 form-group">
								<?=$this->Form->text('label',['class' => 'form-control']) ?><br>
							</div>

							<div class="clearfix"></div>
							<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ステータス</label></div>
							<div class="col-md-10 col-sm-12 col-xs-12 form-group">
								<div class="radio">
									<?=$this->Form->radio('status', Configure::read('status'));?>
								</div>
							</div>

							<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">入力必須</label></div>
							<div class="col-md-10 col-sm-12 col-xs-12 form-group">
								<div class="checkbox">
									<label><?=$this->Form->checkbox('required', Configure::read('form_require'));?> 必須</label>
								</div>
							</div>

							<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">タイプ</label></div>
							<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
								<div class="form-group">
									<?=$this->Form->select('type', Configure::read('form_type'), ['class' => 'form-control'])?>
								</div>
							</div>

							<div class="clearfix"></div>

							<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">データ</label></div>
							<div class="col-md-10 col-sm-12 col-xs-12 form-group">
								<?=$this->Form->text('options',['class' => 'form-control']) ?><br>
								ラジオボタン、チェックボックス、プルダウン選択ボックスの選択値を「|」で区切って入力してください。
							</div>

							<div class="clearfix"></div>

							<div class="ln_solid"></div>


							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<button type="submit" class="btn btn-success">登録する</button>
								</div>
							</div>

						<?=$this->Form->end() ?>
					</div>
				</div>

			</div>
		</div>
