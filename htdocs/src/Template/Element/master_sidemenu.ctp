<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><span class="label label-danger">システム管理画面</span></h3>
                <ul class="nav side-menu">
                  <li><a href="/master/agencies"><i class="fa fa-building"></i> 団体一覧</a></li>
                  <li><a href="/master/agencies/add"><i class="fa fa-building"></i> 団体登録</a></li>
                  <li><a href="/master/users/"><i class="fa fa-user"></i> 会員情報 </a></li>
                  <li><a href="/master/users/add"><i class="fa fa-user"></i> 会員登録 </a></li>
                  <li><a href="/master/users/import"><i class="fa fa-user"></i> 会員データインポート </a></li>
                  <li><a href="/master/setting/edit"><i class="fa fa-cog"></i> 基本情報設定 </a></li>
                </ul>
              </div>
            </div>