<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;

$this->Form->templates(['dateWidget' => '<div class="form-group">{{year}}年</div><div class="form-group">{{month}}月</div><div class="form-group">{{day}} 日 </div>']);				
$dateAttr = [
	'monthNames' => false,
	'year' => [
		'start' => 1960,
		'end'   => date('Y') - 20,
		'class' => 'form-control',
	],
	'month' => [
		'class' => 'form-control',
	],
	'day' => [
		'class' => 'form-control',
	],
	'value' => $user->birthday,
];

$lock = ($policy == 'user')? true : false;

?>


	<?=$this->Form->create($user,['data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) ?>
	<h4>基本プロフィール</h4>
	<?php if($policy == 'master'): ?>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">団体</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('agency_id', $agencies, ['value' => $user->agency_id, 'class' => 'form-control'])?>
		</div>
	</div>

	<?php endif; ?>
	<?php if(!$lock):?>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ステータス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('status', [1 => '有効　', 0 => '無効　'], ['value' => $user->status]);?>
		</div>
	</div>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">メールアドレス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->text('email',['class' => 'form-control', 'value' => $user->email]);?>
		</div>
	</div>
	<?php endif;?>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">プロフィール画像</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<?php if($user->image): ?>
			<img src="<?=$user->image?>" width="200"><br>
			<?=$this->Form->checkbox('image_del', ['class' => 'form-control'])?> 削除<br>
		<?php endif;?>
		<div class="form-group"><?=$this->Form->file('image', ['class' => 'form-control'])?></div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">氏名</label></div>
	<div class="col-md-5 col-sm-12 col-xs-12 form-group"><?=($lock)? $user->firstname : $this->Form->text('firstname',['class' => 'form-control', 'value' => $user->firstname]) ?></div>
	<div class="col-md-5 col-sm-12 col-xs-12 form-group"><?=($lock)? $user->lastname : $this->Form->text('lastname',['class' => 'form-control', 'value' => $user->lastname]) ?></div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">カタカナ</label></div>
	<div class="col-md-5 col-sm-12 col-xs-12 form-group"><?=($lock)? $user->firstname_kana : $this->Form->text('firstname_kana',['class' => 'form-control', 'value' => $user->firstname_kana]) ?></div>
	<div class="col-md-5 col-sm-12 col-xs-12 form-group"><?=($lock)? $user->lastname_kana : $this->Form->text('lastname_kana',['class' => 'form-control', 'value' => $user->lastname_kana]) ?></div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">性別</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div id="gender" class="btn-group" data-toggle="buttons">
			<?php if($lock):?>
			<?=Configure::read('sex')[$user->sex];?>
			<?php else:?>
			<?=$this->Form->radio('sex', Configure::read('sex'), ['value' => $user->sex, 'label' => ['class' => 'btn btn-default', 'data-toggle-class' => 'btn-primary', 'data-toggle-passive-class' => 'btn-primary-active']]);?>
			<?php endif; ?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">生年月日</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
			<?php if($lock):?>
			<?php 
				$date = new Date($user->birthday);
				echo $date->format('Y年m月d日');
			?>
			<?php else:?>
			<?=$this->Form->date('birthday', $dateAttr)?>
			<?php endif; ?>

		
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">身長／体重</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('height',['class' => 'form-control', 'value' => $user->height]) ?>&nbsp;cm&nbsp;<?=$this->Form->text('weight',['class' => 'form-control', 'value' => $user->weight]) ?>&nbsp;kg
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">血液型</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12">
		<div id="bloodtype" class="btn-group" data-toggle="buttons">
			<?=$this->Form->radio('blood_type', Configure::read('blood_type'), ['value' => $user->blood_type, 'label' => ['class' => 'btn btn-default', 'data-toggle-class' => 'btn-primary', 'data-toggle-passive-class' => 'btn-default']]);?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">最終学歴</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('edu_back', Configure::read('edu_back'), ['value' => $user->edu_back, 'class' => 'form-control'])?>
		</div>
		<div class="form-group">
			<?=$this->Form->select('edu_back_type', Configure::read('edu_back_type'), ['value' => $user->edu_back_type, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">職業</label></div>
	<div class="form-group col-md-10 col-sm-12 col-xs-12">
		<div class="checkbox">
			<?=$this->Form->radio('job', Configure::read('job'), ['value' => $user->job]);?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">年収</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('income',['value' => $user->income, 'class' => 'form-control']) ?>&nbsp;万円
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（都道府県）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-inline">
		<div class="form-group">
			<?=$this->Form->select('live_pref', Configure::read('pref'), ['value' => $user->live_pref, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（市区町村）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('live_city',['value' => $user->live_city, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（番地以下）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('live_address',['value' => $user->live_address, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="tel">電話番号</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->text('tel',['class' => 'form-control']);?>
			*他の会員には表示されません。
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">続柄</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('relationship', Configure::read('relationship'), ['value' => $user->relationship, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">職場（都道府県）</label></div>
	<div class="form-group col-md-10 col-sm-12 col-xs-12 form-inline">
		<div class="form-group">
			<?=$this->Form->select('work_pref', Configure::read('pref'), ['value' => $user->work_pref, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">職場（市区町村）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('work_city',['value' => $user->work_city, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">職業備考</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('work_remarks',['value' => $user->work_remarks, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="ln_solid"></div>

	<h4>趣味／趣向</h4>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">国籍</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('nationality',['value' => $user->nationality, 'class' => 'form-control']) ?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">婚歴</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('history', Configure::read('history'), ['value' => $user->history, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">離婚理由</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('reason', ['value' => $user->reason, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">子供</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('children', Configure::read('children'), ['value' => $user->children, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">趣味</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('hobby', ['value' => $user->hobby, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">資格</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('qualification', ['value' => $user->qualification, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">本人資産</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('asset', ['value' => $user->asset, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">その他資産</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('asset_other', ['value' => $user->asset_other, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">お酒／たばこ</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			お酒
			<?=$this->Form->select('alcohol', Configure::read('alcohol'), ['value' => $user->alcohol, 'class' => 'form-control'])?>
		</div>
		<div class="form-group">
			たばこ
			<?=$this->Form->select('tobacco', Configure::read('tobacco'), ['value' => $user->tobacco, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">宗教／宗教名</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('religion', Configure::read('exist'), ['value' => $user->religion, 'class' => 'form-control'])?>
		</div>
		<div class="form-group">
			<?=$this->Form->text('religion_name', ['value' => $user->religion_name, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="ln_solid"></div>

	<h4>お相手への希望</h4>

	
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">お相手への希望</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('desired_message', ['value' => $user->desired_message, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">希望年齢</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('desired_age_low', Configure::read('age_option'), ['value' => $user->desired_age_low, 'class' => 'form-control'])?>〜
			<?=$this->Form->select('desired_age_high', Configure::read('age_option'), ['value' => $user->desired_age_high, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">希望年収</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('desired_salary_low', Configure::read('income_option'), ['value' => $user->desired_salary_low, 'class' => 'form-control'])?>〜
			<?=$this->Form->select('desired_salary_high', Configure::read('income_option'), ['value' => $user->desired_salary_high, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">地域（都道府県）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-inline">
		<div class="form-group">
			<?=$this->Form->select('desired_pref', Configure::read('pref'), ['value' => $user->desired_pref, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">地域（市区町村）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('desired_city', ['value' => $user->desired_city, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">家族同居</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			相手家族同居
			<?=$this->Form->select('desired_family01', Configure::read('prop'), ['value' => $user->desired_family01, 'class' => 'form-control'])?>
		</div>
		<div class="form-group">
			自分家族同居
			<?=$this->Form->select('desired_family02', Configure::read('desired_family02'), ['value' => $user->desired_family02, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">結婚後子供</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('about_child', Configure::read('hope'), ['value' => $user->about_child, 'class' => 'form-control'])?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">養子について</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('about_adopted', Configure::read('hope_b'), ['value' => $user->about_adopted, 'class' => 'form-control'])?>
		</div>
	</div>
	
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">再婚者</label></div>
	<div class="form-inline col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="form-group">
			<?=$this->Form->select('about_remarrior', Configure::read('prop'), ['value' => $user->about_remarrior, 'class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="ln_solid"></div>

	<?php if(!empty($formParams)):?>
	<h4>追加プロフィール</h4>
	<?=$this->Matching->generateForm($formParams);?>
	<?php endif; ?>

	<h4>PR／コメント</h4>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">自己PR</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->textarea('pr01', ['value' => $user->pr01, 'class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">カウンセラーPR</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?php if($lock):?>
			<?=h($user->pr02) ?>
			<?php else: ?>
			<?=$this->Form->textarea('pr02', ['value' => $user->pr02, 'class' => 'form-control']) ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="clearfix"></div>
	<div class="ln_solid"></div>

	<h4>パスワード変更</h4>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">パスワード</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group"><div class="radio"><?=$this->Form->text('password',['value' => '', 'class' => 'form-control']);?></div></div>

	<div class="clearfix"></div>
	<div class="ln_solid"></div>

	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<?=$this->Form->button('登録する', ['type' => 'submit','class' => 'btn btn-success']) ?>
		</div>
	</div>

	<?=$this->Form->end() ?>
