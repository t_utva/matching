<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<ul class="nav side-menu">
			<li><a href="/mypage/profiles/"><i class="fa fa-user"></i> マイプロフィール </a></li>
			<li><a href="/mypage/searches/"><i class="fa fa-search"></i> プロフィール検索 </a></li>
			<li><a href="/mypage/messages/index"><i class="fa fa-comments-o"></i> メッセージ </a></li>
			<?php if($this->request->params['loginUser']['agency']['flg_match'] == 1):?>
			<li><a href="/mypage/matching/"><i class="fa fa-comments-o"></i> お見合申込一覧 </a></li>
			<?php endif; ?>
			<li><a href="/mypage/favorite/"><i class="fa fa-star"></i> お気に入り一覧 </a></li>
		</ul>
	</div>
</div>