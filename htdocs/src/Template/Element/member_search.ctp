<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
<?=$this->Form->create(null,['url' => ['action' => 'result'], 'data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">キーワード</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?=$this->Form->input('keyword', ['label' => false, 'class' => 'form-control col-md-7 col-xs-12']);?>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">性別</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="radio">
			<?=$this->Form->radio('sex', [1 => '男性　', 2 => '女性　']);?>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">年齢</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?=$this->Form->select('desired_age', Configure::read('age_option'), ['value' => null, 'class' => 'form-control'])?>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">年収</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?=$this->Form->select('desired_salary', Configure::read('income_option'), ['value' => null, 'class' => 'form-control'])?>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">地域</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?=$this->Form->select('desired_pref', Configure::read('pref'), ['value' => null, 'class' => 'form-control'])?>
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 col-sm-3 col-xs-12 control-label">職業<br><small class="text-navy">複数選択可</small></label>
	<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="checkbox form-inline">
			<?=$this->Form->input(
						  'job', 
						  [
							'options' => Configure::read('job'), 
							'type'   => 'select', 
							'multiple' => 'checkbox', 
							'label'   => false,
							'templates' => [
							  'nestingLabel' => '{{hidden}}<label{{attrs}} class="checkbox-inline">{{input}}{{text}}</label>&nbsp;&nbsp;',
							],
						  ]
			);?>
		</div>
	</div>
</div>

<div class="ln_solid"></div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		<button type="submit" class="btn btn-success">検索する</button>
	</div>
</div>

<?=$this->Form->end() ?>
