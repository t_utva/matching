<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Configure;
use Cake\I18n\Date;

$this->Form->templates(['dateWidget' => '<div class="form-group">{{year}}年</div><div class="form-group">{{month}}月</div><div class="form-group">{{day}} 日 </div>']);        
$dateAttr = [
  'monthNames' => false,
  'year' => [
    'start' => 1960,
    'end'   => date('Y') - 20,
    'class' => 'form-control',
  ],
  'month' => [
    'class' => 'form-control',
  ],
  'day' => [
    'class' => 'form-control',
  ],
  'value' => $user->birthday,
];

$lock = ($policy == 'user')? true : false;
$date = new Date($user->birthday);
?>
                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <!--<h4 class="brief"><i>Digital Strategist</i></h4>-->
                            <div class="col-xs-7">
                              <h2><?=$user->firstname?> <?=$user->lastname?> <small>(<?=(int)((date('Ymd')-$date->format('Ymd'))/10000);?>歳)</small></h2>
                              <ul class="list-unstyled">
                                <li><span class="glyphicon glyphicon-yen" aria-hidden="true"></span> <?=Configure::read('income_option')[$this->Matching->convIncome($user->income,'toParam')]?></li>
                                <li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?=Configure::read('pref')[$user->live_pref]?><?=$user->live_city?></li>
                                <li><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <?=Configure::read('job')[$user->job]?></li>
                                <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?=$user->height?>cm / <?=$user->weight?>kg / <?=Configure::read('blood_type')[$user->blood_type]?></li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="<?=$this->Matching->getImagePath($user->image,'/images/user.png')?>" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis">
                              <button type="button" class="btn btn-primary btn-xs" onclick="javascript:location.href='/mypage/users/detail/<?=$user->user_code?>'">
                                <i class="fa fa-user"> </i> プロフィールを見る
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>