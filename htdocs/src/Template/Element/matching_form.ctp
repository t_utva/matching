<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;

?>


	<?=$this->Form->create($matching,['data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ステータス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<?php if($policy == 'master' || $policy == 'agency'): ?>
		<div class="radio">
			<?=$this->Form->radio('status', Configure::read('matching'));?>
		</div>
		<?php else: ?>
			<?=Configure::read('matching')[$matching->status];?>
		<?php endif; ?>
	</div>

	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">申込内容</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=nl2br(h($matching->message)) ?>
		</div>
	</div>
	<?php if($policy == 'master' || $policy == 'agency'): ?>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">メモ</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->textarea('memo', ['class' => 'form-control']);?>
		</div>
	</div>
	<?php endif; ?>
	<div class="clearfix"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<?php if($policy == 'master' || $policy == 'agency'): ?>
			<?=$this->Form->button('登録する', ['type' => 'submit','class' => 'btn btn-success']) ?>
			<?php else: ?>
			<button type="button" class="btn btn-success" onclick="javascript:history.back(1)">戻る</button>
			<?php endif; ?>
		</div>
	</div>

	<?=$this->Form->end() ?>
