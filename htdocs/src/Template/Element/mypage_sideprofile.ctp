<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="profile clearfix">
	<div class="profile_pic">
		<img src="<?=($this->request->params['loginUser']['image'])? $this->request->params['loginUser']['image']: '/images/user.png'?>" class="img-circle profile_img">
	</div>
	<div class="profile_info">
		<span>ようこそ,</span>
		<h2><?=$this->request->params['loginUser']['firstname'] . ' ' .$this->request->params['loginUser']['lastname']?></h2>
	</div>
	<div class="clearfix"></div>
</div>
