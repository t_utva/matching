<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;
$params = $this->request->params;
?>
<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
	<div class="profile_img">
		<div id="crop-avatar">
			<!-- Current avatar -->
			<img src="<?=$this->Matching->getImagePath($user->image,'/images/user.png')?>" alt="" class="img-responsive avatar-view">
		</div>
	</div>
	<h3><?=$user->firstname?> <?=$user->lastname?></h3>
	<ul class="list-unstyled user_data">
		<?php $date = new Date($user->birthday); ?>
		<li><i class="fa fa-heart user-profile-icon"></i> <?=$date->format('Y年m月d日')?>生</li>
		<li><i class="fa fa-group user-profile-icon"></i> <?=$user->agency->name?></li>
	</ul>
	<?php if($params['controller'] == 'Profiles' && $params['action'] == 'index'): ?>
	<a href="/mypage/profiles/edit" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>プロフィール編集</a><br />
	<!--<a class="btn btn-default"><i class="fa fa-eye m-right-xs"></i>プロフィール確認</a><br />-->
	<?php elseif($params['controller'] == 'Users' && $params['action'] == 'detail'): ?>
	<button class="btn btn-success btn-favorite" data-id="<?=$user->id?>"><i class="fa fa-edit m-right-xs"></i>お気に入りに登録</button><br />
	<?php if($params['loginUser']['agency']['flg_message'] == 1): ?>
	<a href="/mypage/messages/message/<?=$msgRoomName?>" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>メッセージを送る</a><br />
	<?php endif; ?>
	<?php if($params['loginUser']['agency']['flg_match'] == 1): ?>
	<a href="/mypage/matching/apply/<?=$user->user_code?>" class="btn btn-default"><i class="fa fa-check-square m-right-xs"></i>お見合いを申し込む</a><br />
	<?php endif; ?>
	<!--<a class="btn btn-default"><i class="fa fa-eye m-right-xs"></i>プロフィール確認</a><br />-->
	<?php endif; ?>
</div>