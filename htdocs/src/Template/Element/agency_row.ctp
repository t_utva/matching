<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;
?>
		<tr>
			<td><?=$agency->id?></td>
			<td>
				<ul class="list-inline">
					<li>
						<img src="/images/user.png" class="avatar" alt="Avatar">
					</li>
				</ul>
			</td>
			<td><a href="/master/agencies/edit/<?=$agency->agency_code?>"><?=$agency->name?>(<?=$agency->name_kana?>)</a></td>
			<td><?=$agency->login_id?></td>
			<td><?=$agency->mail?></td>
			<td><?=$agency->tel?></td>
			<td><?=Configure::read('status')[$agency->status]?></td>
			<td><?=Configure::read('approve')[$agency->approve]?></td>
			<td>
				<?php $date = new Date($agency->last_login_date); ?>
				<?=$date->format('Y年m月d日');?>(<?=Configure::read('week')[$date->format('w')]?>) <?=$date->format('H:i');?>
			</td>
			<td>
				<a href="/master/agencies/edit/<?=$agency->agency_code?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> 更新 </a>
			</td>
		</tr>