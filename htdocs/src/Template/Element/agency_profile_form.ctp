<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;

?>


	<?=$this->Form->create($agency,['data-parsley-validate' => 'data-parsley-validate', 'class' => 'form-horizontal form-label-left']) ?>

	<?php if($policy == 'master'): ?>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ステータス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('approve', Configure::read('approve'));?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ステータス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('status', Configure::read('status'));?>
		</div>
	</div>
	<?php endif; ?>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">ログインID</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=($policy == 'master')? $this->Form->text('login_id',['class' => 'form-control']) : $agency->login_id; ?>
		</div>
	</div>	
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">メールアドレス</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->text('mail', ['class' => 'form-control']);?>
		</div>
	</div>
	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">パスワード</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->text('password',['class' => 'form-control', 'value' => '']);?>
		</div>
	</div>


	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">団体名</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->text('name', ['class' => 'form-control']);?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">団体名（カナ）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->text('name_kana', ['class' => 'form-control']);?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（郵便番号）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('zip',['class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（都道府県）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-inline">
		<div class="form-group">
			<?=$this->Form->select('pref', Configure::read('pref'), ['class' => 'form-control'])?>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（市区町村）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('city',['class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">住所（番地以下）</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->text('address',['class' => 'form-control']) ?>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">団体PR</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12">
		<div class="form-group">
			<?=$this->Form->textarea('pr', ['class' => 'form-control']) ?>
		</div>
	</div>

	<div class="clearfix"></div>

	<h4>会員向け設定</h4>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">お見合い機能</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('flg_match', Configure::read('status'));?>
			<small>* 許可すると会員同士が直接連絡可能となります。許可しない場合は全てのメッセージが管理者宛に送信されます。</small>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">会員間メッセージ機能</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('flg_message', Configure::read('status'));?>
			<small>* 許可すると会員同士が直接連絡可能となります。許可しない場合は全てのメッセージが管理者宛に送信されます。</small>
		</div>
	</div>

	<div class="form-group col-md-2 col-sm-12 col-xs-12"><label class="control-label" for="first-name">他団体連携機能</label></div>
	<div class="col-md-10 col-sm-12 col-xs-12 form-group">
		<div class="radio">
			<?=$this->Form->radio('flg_crossover', Configure::read('status'));?>
			<small>* 連携許可すると、許可団体間で会員の交流が可能となります。</small>
		</div>
	</div>


	<div class="ln_solid"></div>


	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<?=$this->Form->button('登録する', ['type' => 'submit','class' => 'btn btn-success']) ?>
		</div>
	</div>

	<?=$this->Form->end() ?>
