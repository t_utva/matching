<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<h3><?=$this->request->params['loginUser']['name']?>&nbsp;<span class="label label-danger">管理画面</span></h3>
		<ul class="nav side-menu">
		<li><a href="/agency/users/"><i class="fa fa-user"></i> 会員情報 </a></li>
		<li><a href="/agency/users/add"><i class="fa fa-user"></i> 会員登録 </a></li>
		<?php if($this->request->params['loginUser']['flg_match'] == 1):?>
		<li><a href="/agency/matching/"><i class="fa fa-comments-o"></i> お見合申込一覧 </a></li>
		<?php endif; ?>
		<li><a href="/agency/messages/index"><i class="fa fa-comments-o"></i> メッセージ </a></li>
		<li><a href="/agency/form/"><i class="fa fa-list-alt"></i> 会員設定項目 </a></li>
		<li><a href="/agency/form/add"><i class="fa fa-list-alt"></i> 会員設定項目登録 </a></li>
		<li><a href="/agency/setting/edit"><i class="fa fa-cog"></i> 基本情報設定 </a></li>
		</ul>
	</div>
</div>