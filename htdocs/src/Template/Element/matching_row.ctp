<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;
?>
		<tr>
			<td><?=$matching->id?></td>
			<td>
				<ul class="list-inline">
					<?php if($policy == 'agency'): ?>
					<li>
						<img src="<?=($matching->applicant_user['image'])? $matching->applicant_user['image']: '/images/user.png'; ?>" class="avatar" alt="Avatar">
					</li>
					<li>
						<img src="<?=($matching->passive_user['image'])? $matching->passive_user['image'] : '/images/user.png'?>" class="avatar" alt="Avatar">
					</li>
					<?php else: ?>
					<li>
						<img src="<?=($matching->user->image)? $matching->user->image : '/images/user.png'?>" class="avatar" alt="Avatar">
					</li>
					<?php endif; ?>
				</ul>
			</td>
			<?php if($policy == 'agency'): ?>
			<td><?=$matching->applicant_user['firstname']?><?=$matching->applicant_user['lastname']?></td>
			<td><?=$matching->passive_user['firstname']?><?=$matching->passive_user['lastname']?></td>
			<?php else: ?>
			<td><?=$matching->user->firstname?><?=$matching->user->lastname?></td>
			<?php endif; ?>
			<td>
				<?php $date = new Date($matching->created); ?>
				<?=$date->format('Y年m月d日');?>(<?=Configure::read('week')[$date->format('w')]?>) <?=$date->format('H:i');?>
			</td>
			<td><?=Configure::read('matching')[$matching->status]?></td>
			<td>
				<a href="/<?=($policy == 'user')? 'mypage':$policy;?>/matching/edit/<?=$matching->matching_code?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> 確認 </a>
				<?php if($policy == 'agency'): ?>
				<a href="/<?=$policy?>/users/message/<?=$matching->applicant_user['user_code']?>" class="btn btn-primary btn-xs"><i class="fa fa-comments-o"></i> 申込元 </a>
				<a href="/<?=$policy?>/users/message/<?=$matching->passive_user['user_code']?>" class="btn btn-primary btn-xs"><i class="fa fa-comments-o"></i> 申込先 </a>
				<?php endif; ?>
			</td>
		</tr>