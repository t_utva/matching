<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\Date;
?>
		<tr>
			<td><?=$user->id?></td>
			<td>
				<ul class="list-inline">
					<li>
						<img src="<?=$this->Matching->getImagePath($user->image,'/images/user.png')?>" alt="" width="50" class="img-responsive avatar-view">
					</li>
				</ul>
			</td>
			<?php if($policy == 'master'): ?><td><a href="/<?=$policy?>/agencies/edit/<?=$user->agency->agency_code?>"><?=$user->agency->name?></a></td><?php endif; ?>
			<td><a href="/<?=$policy?>/users/edit/<?=$user->user_code?>"><?=$user->firstname?><?=$user->lastname?></a></td>
			<td><?=Configure::read('sex')[$user->sex]?></td>
			<td>
				<?php $date = new Date($user->birthday); ?>
				<?=(int)((date('Ymd')-$date->format('Ymd'))/10000);?>歳
			</td>
			<td><?=$user->height?>cm / <?=$user->weight?>kg / <?=Configure::read('blood_type')[$user->blood_type]?></td>
			<td><?=Configure::read('job')[$user->job]?></td>
			<td><?=$user->income?>万円</td>
			<td><?=Configure::read('pref')[$user->live_pref]?><?=$user->live_city?></td>
			<td>
				<?php $date = new Date($user->last_login_date); ?>
				<?=$date->format('Y年m月d日');?>(<?=Configure::read('week')[$date->format('w')]?>) <?=$date->format('H:i');?>
			</td>
			<td>
				<a href="/<?=$policy?>/users/edit/<?=$user->user_code?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> 更新 </a>
				<?php if($policy == 'agency' || $policy == 'master'):?>
				<a class="btn btn-danger btn-xs" data-toggle="modal" data-target=".user-remove_<?=$user->user_code?>"><i class="fa fa-trash-o"></i> 削除 </a>
				<div class="modal fade user-remove_<?=$user->user_code?>" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h4 class="modal-title" id="myModalLabel2">削除してもよろしいでしょうか？</h4>
							</div>
							<div class="modal-body">
								<h4><?=$user->firstname?><?=$user->lastname?></h4>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" onclick="javascript:location.href='/<?=$policy?>/users/delete/<?=$user->user_code?>'">削除する</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if($policy == 'agency'): ?>
				<a href="/<?=$policy?>/users/message/<?=$user->user_code?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> メッセージ </a>
				<?php endif; ?>
			</td>
		</tr>