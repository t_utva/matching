<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
	<tr>
		<td><?=$form->id?></td>
		<td><a href="/agency/form/edit/<?=$form->form_code?>"><?=$form->label?></a></td>
		<td><?=Configure::read('form_type')[$form->type]?></td>
		<td><?=Configure::read('form_require')[$form->required]?></td>
		<td><?=Configure::read('status')[$form->status]?></td>
		<td>
			<a href="/agency/form/edit/<?=$form->form_code?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> 更新 </a>
			<a href="/agency/form/delete/<?=$form->form_code?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> 削除 </a>
		</td>
	</tr>