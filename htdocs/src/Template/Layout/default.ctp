<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$title?> | 互助会システム</title>
    <?php echo $this->Html->css([
      '/vendors/bootstrap/dist/css/bootstrap.min.css',
      '/vendors/font-awesome/css/font-awesome.min.css',
      '/vendors/nprogress/nprogress.css',
      '/vendors/iCheck/skins/flat/green.css',
      '/vendors/google-code-prettify/bin/prettify.min.css',
      '/vendors/select2/dist/css/select2.min.css',
      '/vendors/switchery/dist/switchery.min.css',
      '/vendors/starrr/dist/starrr.css',
      '/vendors/bootstrap-daterangepicker/daterangepicker.css',
      '/vendors/pnotify/dist/pnotify.css',
      '/vendors/pnotify/dist/pnotify.buttons.css',
      '/vendors/pnotify/dist/pnotify.nonblock.css',
      '/css/gentelella/custom.min.css'
    ]); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php 
              switch($policy){
                case 'user' : 
                  $url = 'mypage';
                  break;
                case 'agency' :
                  $url = 'agency';
                  break;
                case 'master' : 
                  $url = 'master';
                  break;
              }

            ?>


              <a href="/<?=$url?>/" class="site_title"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> <span>互助会システム</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php 
              switch($policy){
                case 'user':
                  echo $this->element('mypage_sideprofile');
                break;
                case 'agency':
                  echo $this->element('agency_sideprofile');
                break;
                case 'master':
                  echo $this->element('master_sideprofile');
                break;
              }
            ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php 
              switch($policy){
                case 'user':
                  echo $this->element('mypage_sidemenu');
                break;
                case 'agency':
                  echo $this->element('agency_sidemenu');
                break;
                case 'master':
                  echo $this->element('master_sidemenu');
                break;
              }
            ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php 
          switch($policy){
            case 'user':
              echo $this->element('mypage_topmenu');
            break;
            case 'agency':
              echo $this->element('agency_topmenu');
            break;
          }
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?=$title?></h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            エフコネクト 2017
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php echo $this->Html->script([
      '/vendors/jquery/dist/jquery.min.js',
      '/vendors/bootstrap/dist/js/bootstrap.min.js',
      '/vendors/fastclick/lib/fastclick.js',
      '/vendors/nprogress/nprogress.js',
      '/vendors/raphael/raphael.min.js',
      '/vendors/morris.js/morris.min.js',
      '/vendors/moment/min/moment.min.js',
      '/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
      '/vendors/bootstrap-daterangepicker/daterangepicker.js',
      '/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js',
      '/vendors/iCheck/icheck.min.js',
      '/vendors/moment/min/moment.min.js',
      '/vendors/jquery.hotkeys/jquery.hotkeys.js',
      '/vendors/google-code-prettify/src/prettify.js',
      '/vendors/jquery.tagsinput/src/jquery.tagsinput.js',
      '/vendors/switchery/dist/switchery.min.js',
      '/vendors/select2/dist/js/select2.full.min.js',
//      '/vendors/parsleyjs/dist/parsley.min.js',
      '/vendors/autosize/dist/autosize.min.js',
      '/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
      '/vendors/starrr/dist/starrr.js',
//      '/vendors/pnotify/dist/pnotify.js',
//      '/vendors/pnotify/dist/pnotify.buttons.js',
//      '/vendors/pnotify/dist/pnotify.nonblock.js',
      '/js/gentelella/custom.js',
      '/js/my.js'
    ]); ?>
  </body>
</html>
