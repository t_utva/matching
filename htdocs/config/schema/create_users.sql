CREATE TABLE customers (
    id 				SERIAL       PRIMARY KEY,
    status 			INT(2)       NULL,
    firstname 		VARCHAR(255) NULL, 
    lastname 		VARCHAR(255) NULL,
    firstname_kana 	VARCHAR(255) NULL, 
    lastname_kana 	VARCHAR(255) NULL,
    sex 			INT(2)       NULL,
    birthday		DATETIME	 NULL,
    height			INT(4)		 NULL,
    weight			INT(4)		 NULL,
    blood_type		INT(2)		 NULL,
    edu_back		INT(2)		 NULL,
    edu_back_type	INT(2)		 NULL,
    income			INT(11)		 NULL,
    live_pref		INT(3)		 NULL,
    live_city		VARCHAR(255) NULL,
    live_address	VARCHAR(255) NULL,
    relationship	INT(3)		 NULL,
    work_pref		INT(3)		 NULL,
    work_city		VARCHAR(255) NULL,
    work_remarks	VARCHAR(255) NULL,
    nationality		VARCHAR(255) NULL,
    history			INT(3)		 NULL,
    reason			varchar(255) NULL,
    children		int(3)		 NULL,
    hobby			varchar(255) NULL,
    qualification	varchar(255) NULL,
    asset			varchar(255) NULL,
    asset_other		varchar(255) NULL,
    alcohol			int(3)		 NULL,
    tobacco			int(3)		 NULL,
    religion		int(2)		 NULL,
    religion_name	varchar(255) NULL,

    desired_message varchar(255) NULL,
    desired_age     int(2)       NULL,
    desired_salary  int(2)       NULL,
    desired_pref    int(2)       NULL,
    desired_city    varchar(255) NULL,
    desired_family01 int(2)      NULL,
    desired_family02 int(2)      NULL,
    about_child     int(2)       NULL,
    about_adopted   int(2)       NULL,
    about_remarrior int(2)       NULL,

    pr01            varchar(4000) NULL,
    pr02            varchar(4000) NULL,

    email           VARCHAR(255) NOT NULL, 
    password        VARCHAR(255) NOT NULL,
    salt            varchar(255) NOT NULL,
    role      VARCHAR(20)  NOT NULL,
    created   TIMESTAMP    NOT NULL DEFAULT NOW(), 
    modified  TIMESTAMP,
    PRIMARY KEY  (id)

);