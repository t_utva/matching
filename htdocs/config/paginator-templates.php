<?php
return [
    'nextActive' => '<li class="next"><a rel="next" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="disabled"><a>{{text}}</a></li>',
    'prevActive' => '<li class="prev"><a rel="prev" href="{{url}}">{{text}}</a></li>',
    'prevDisabled' => '<li class="disabled"><a>{{text}}</a></li>',
    'number' => '<li><a href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="current"><a>{{text}}</a></li>',
];
